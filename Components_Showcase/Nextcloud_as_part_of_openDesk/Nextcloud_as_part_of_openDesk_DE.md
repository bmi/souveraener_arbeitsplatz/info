**_Please find the English version [here](./Nextcloud_as_part_of_openDesk_EN.md)_.**

# Nextcloud-Hub - Was bietet die Open-Source-Software?

## Sichere Zusammenarbeit mit Nextcloud in openDesk

In openDesk bietet Nextcloud Echtzeit-Zugriff auf Dateien mit Desktop-Synchronisierung, mobilem Zugriff und der Möglichkeit, Dateien zu bearbeiten, zu teilen, zu kommentieren und Aktivitäten zu verfolgen. Nextcloud bietet ein benutzerfreundliches Erlebnis, das hohen Standards für Barrierefreiheit in der gesamten Weboberfläche, den Desktop-Clients und den mobilen Apps folgt und eine Zusammenarbeit in Echtzeit und einen nahtlosen Datenzugriff über alle Geräte hinweg gewährleistet, unabhängig vom Standort.

**Die wichtigsten Funktionen von Nextcloud Files:**

-	Sichere Freigabe von Dateien und Ordnern
-	Flexible Zugriffsberechtigungen
-	Verstärkter Schutz von Links und Verfallsdaten
-	Dateisperre zum Schutz von Dateien vor Änderungen während der Bearbeitung
-	Synchronisierung von Desktop und Handy
-	Bearbeitung und gemeinsame Bearbeitung von Dokumenten über die Integration von Office-Suiten
-	Besitzübertragungen für Dateien und Ordner
-	Versionskontrolle mit der Möglichkeit, alte Versionen wiederherzustellen
-	Client-seitiger Schutz von Dateien und Ende-zu-Ende-Verschlüsselung
-	File-Drop-Funktion, mit der andere Personen Dateien über einen Link in einen sicheren Ordner hochladen können
-	Konfiguration der Dateieigenschaften für eine beliebige Anzahl von Benutzenden oder Gruppen

![Screenshot von Nextcloud Files](../_media/1%20Nextcloud%20Files.png)

## Nextcloud: Integrierte Plattform für die Zusammenarbeit mit Datenschutz im Mittelpunkt

_Nextcloud_ entwickelt umfassende und vielseitige Lösungen, die darauf abzielen, eine sichere, private und kontrollierte Umgebung für die Synchronisierung und gemeinsame Nutzung von Dateien, die Zusammenarbeit im Team und die Produktivität zu schaffen. Die Full-Stack-Plattform Nextcloud Hub ist eine 100%ige Open-Source-Lösung, die selbst gehostet wird und sich als einzelne oder föderierte Instanz leicht in bestehende Speicher- und Benutzerverzeichnisse integrieren lässt.

Das gesamte Hub umfasst vier wichtige App-Module, die eine einheitliche Plattform für kollaborative Arbeitsabläufe bieten:

-	Nextcloud Files: Dateisynchronisierungs- und Freigabeplattform für die sichere Verwaltung und Freigabe von Dokumenten.
-	Nextcloud Groupware: ein Bündel von Produktivitätslösungen für die Zusammenarbeit im TeamL Kalender, Kontakte, Mail und Deck.
-	Nextcloud Talk - sichere Webkonferenzen und Chats mit leistungsstarkem Backend und SIP-Bridge.
-	Nextcloud Office: Nextcloud Text Markdown-Editor und kollaborative Office-Suite, die mit Collabora Online erstellt wurde.

_**openDesk umfasst Nextcloud Files, während Talk, Groupware und Office-Funktionen von anderen Anbietern bereitgestellt werden.**_

![Screenshot von Nextcloud Files](../_media/2_Nextcloud_Files.png)

Die modulare Architektur ermöglicht die Integration von Hub mit Anwendungen von Drittanbietern wie CRM-Software, Office-Suiten, Projektmanagement-Tools und mehr.
Mehr als 200 fertige Anwendungen sind derzeit für die Integration verfügbar.

Die Plattform legt den Schwerpunkt auf Sicherheit mit robusten Verschlüsselungsfunktionen, regelbasierter Dateizugriffskontrolle, strengen Passwortrichtlinien und Brute-Force-Schutz. Die Einfachheit der Konfiguration und Integration senkt nicht nur die Kosten, sondern auch die Risiken, so dass Unternehmen ihre vorhandenen IT-Investitionen effektiv nutzen können.

Nextcloud Hub eignet sich für alle Teams, die sicher auf Daten zugreifen und diese mit Kollegen, Kunden oder Partnern teilen müssen. Unternehmen, die Nextcloud nutzen, sind in verschiedenen Branchen tätig und bieten notwendige Compliance-Tools für regulierte Sektoren wie Finanzen, Behörden, Bildung, Gesundheitswesen und mehr.

Insgesamt zielt Nextcloud Hub darauf ab, eine ganzheitliche Plattform für die Zusammenarbeit zu bieten, bei der Sicherheit, Datenschutz und Kontrolle über die Daten im Vordergrund stehen, ohne dass es zu einer Bindung an einen bestimmten Anbieter und zu Compliance-Risiken kommt.

## Wie Nextcloud zum Aufbau einer privaten, souveränen Plattform beiträgt

Mit Nextcloud können Unternehmen die Sicherheit ihrer Daten in den Vordergrund stellen, indem sie sie auf vertrauenswürdigen Datenservern in ihrem Land unterbringen und so die Kontrolle über die Daten sicherstellen, ohne sich um ausländische Vorschriften sorgen zu müssen.
Angesichts der zunehmenden Digitalisierung bieten die Funktionen von Nextcloud für den sicheren Dokumentenaustausch, wie z.B. File Drop, Gruppenordner mit erweiterten Berechtigungen und persistente Versionierung, eine Lösung, die den Datenschutz und die Sicherheit der Daten bei jedem Verarbeitungsschritt gewährleistet.

-	Vollständige Einhaltung wichtiger regionaler und industrieller Vorschriften wie GDPR, CCPA, HIPAA, FERPA und COPPA
-	Sichere Bereitstellung vor Ort
-	100% offener Quellcode
-	Konsistenter Aktualisierungszyklus und proaktiver Ansatz bei der Schwachstellenanalyse
-	Lokale Daten innerhalb des rechtlichen Rahmens der Organisation
-	Sichere gemeinsame Nutzung und Zusammenarbeit mit flexiblen Berechtigungen, E2EE, File Drop und anderen Tools, die den Komfort für den Benutzenden erhöhen



