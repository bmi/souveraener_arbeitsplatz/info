**_Die Deutsche Version finden Sie [hier](./Collabora_as_part_of_openDesk_DE.md)_**

# Collabora Online: The Open-Source Document Editor - What Does the Open Source Software Offer?

In this series, we are taking a closer look at the different contributors to openDesk. This article, we will further investigate what the Cambridge-based company Collabora Productivity brings to the project, with their flagship product – Collabora Online.

Collabora Online is widely becoming recognized as the solution for secure, collaborative, open-source document editing. It is deployed by private-sector companies, as well as government departments, hospitals, and universities worldwide, making it an ideal match for the openDesk project. Collabora Online is based on LibreOffice, with the company being the largest contributor to the LibreOffice codebase.

![Screenshot from the Collabora application](../_media/Collabora_Online_1.png)

## Collabora Online

Collabora Online is a powerful office suite, offering support for all major document, spreadsheet, and presentation file formats, as well as many legacy documents. Designed to be safe, powerful, and flexible, Collabora Online brings new levels of security to document editing with on-premise, cloud-based, or hybrid solutions all available, whilst also bringing the further adaptability of an open-source solution.

Integrated into the openDesk suite, and having made a number of changes to better fit the specific requirements of the German government, Collabora Online will ensure that openDesk users have the modern, secure document editing environment they expect. Examples of these changes include new integrations with modules such as the Zotero citation and reference management tool, improved accessibility (for example dark mode or screen reader functionality), multi-page floating tables, compact pivot tables, multi-stop gradients, document navigation, and much more.

The Collabora Online document editor within openDesk comes with an open-source theme. Find out more about Collabora Online [here](http://www.collaboraoffice.com/?opendesk).

![Screenshot from the Collabora application](../_media/Collabora_Online_2.png)