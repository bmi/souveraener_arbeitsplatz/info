**_Please find the English version [here](./Collabora_as_part_of_openDesk_EN.md)_.**

# Collabora Online: Der Open-Source-Dokumenten-Editor - Was bietet die Open Source Software?

In dieser Serie werfen wir einen Blick auf die verschiedenen Mitwirkenden von openDesk. Hierbei untersucht dieser Artikel genauer, was das in Cambridge ansässige Unternehmen Collabora Productivity zum Projekt mit ihrem Flaggschiff-Produkt – Collabora Online beiträgt.

Collabora Online wird zunehmend als Lösung für eine sichere, kollaborative, quelloffene Bearbeitung von Dokumenten anerkannt. Es wird sowohl von privaten Unternehmen als auch von Regierungsbehörden, Krankenhäusern und Universitäten weltweit eingesetzt, was es zu einer idealen Ergänzung von openDesk-Projekt macht. Collabora Online basiert auf LibreOffice; das Unternehmen ist der größte Mitwirkende an der LibreOffice-Codebasis.

![Screenshot aus der Collabora Anwendung](../_media/Collabora_Online_1.png)

## Zusammenarbeit online

Collabora Online ist eine leistungsstarke Office-Suite, die alle wichtigen Dateiformate für Dokumente, Tabellenkalkulationen und Präsentationen sowie viele ältere Dokumente unterstützt. Entwickelt, um sicher, leistungsstark und flexibel zu sein, bringt Collabora Online neue Sicherheitsstandards in die Dokumentenbearbeitung. Die Lösung kann On-Premise, Cloud-basiert oder hybrid eingesetzt werden und bietet gleichzeitig die Anpassungsfähigkeit einer Open-Source-Lösung.

Integriert in die openDesk-Suite und mit einer Reihe von Änderungen, um den spezifischen Anforderungen der deutschen Regierung besser gerecht zu werden, sorgt Collabora Online dafür, dass openDesk-Benutzer:innen die moderne, sichere Umgebung für die Bearbeitung von Dokumenten erhalten, die sie erwarten. Beispiele für diese Änderungen ist die Integration von neuen Modulen wie dem Zitier- und Referenzverwaltungstool Zotero, eine verbesserte Zugänglichkeit (z.B. Dunkelmodus oder Screenreader-Funktionalität), mehrseitige schwebende Tabellen, kompakte Pivot-Tabellen, mehrstufige Farbverläufe, Dokumentnavigation und vieles mehr.

Der Collabora Online-Dokumenteneditor in openDesk wird mit einem Open-Source-Theme geliefert. Mehr über Collabora Online erfahren Sie [hier](http://www.collaboraoffice.com/?opendesk).

![Screenshot aus der Collabora Anwendung](../_media/Collabora_Online_2.png)