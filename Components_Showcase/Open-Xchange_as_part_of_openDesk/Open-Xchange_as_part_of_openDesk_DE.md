**_Please find the English version [here](./Open-Xchange_as_part_of_openDesk_EN.md)_.**

# Open-Xchange: Die E-Mail-Komponente für openDesk - Was bietet die Open-Source-Software?

Open-Xchange wurde 2005 mit der Philosophie und dem Ziel gegründet, das Internet frei und offen zu halten, so wie es ursprünglich geplant war. Seitdem haben wir Software entwickelt, die unseren Communities, Partnern und Kund:innen hilft, herausragende und wettbewerbsfähige Dienste anzubieten.
Open-Xchange hat sich immer auf E-Mail konzentriert und bietet heute die weltweit beliebtesten E-Mail-Dienste an, von E-Mail-Servern (DoveCot) bis zu E-Mail-Plattformen (OX App Suite). Diesen Erfolg verdanken wir nicht nur dem gesamten Open-Xchange-Team, das zu zwei Dritteln aus Entwickler:innen besteht, sondern auch unseren großartigen Partner:innen und den weltweit über 220 Mio. Nutzer:innen.

## OX App Suite im Einsatz

Die OX App Suite, die 2005 ursprünglich als Open-Xchange Hosted Edition veröffentlicht wurde, ist mehr als nur eine E-Mail-Plattform. Sie ist eine modular aufgebaute E-Mail-Suite und lässt sich deshalb perfekt mit anderen Produkten kombinieren und so eine übergeordnete Plattform schaffen. Zu der Vielzahl an integrierten Modulen gehören unter anderem:

- **Hochentwickelte E-Mail-Lösung**, die sowohl für private als auch für geschäftliche Nutzer:innen konzipiert ist und Funktionen wie die Verwaltung mehrerer Posteingänge, Delegation, E-Mail-Vorlagen und sogar den Einsatz von künstlicher Intelligenz bietet.
- **Ein innovativer Kalender**, der die Bedürfnisse von privaten und geschäftlichen Nutzer:innen erfüllt und visuelles Terminmanagement, mehrere persönliche, gemeinsame und öffentliche Kalender, Zeitzonenmanagement, Ressourcenmanagement und Delegationsmöglichkeiten in sich vereint.
- **Eine E-Mail- und Dateiverschlüsselung**, die die benutzerfreundlichste und sicherste Verschlüsselung überhaupt ist. Mit einem Mausklick können Benutzer:innen erst eine komplette E-Mail (E-Mail und Anhänge) verschlüsseln und danach das Profil transparent verschlüsseln. Eine Verschlüsselung, die funktioniert, weil sie so einfach ist.

![Screenshot aus der OX-Anwendung](../_media/OX%20App%20Suite%20as%20a%20product.png)

**Die Kernanwendungsfälle sind:**

-	Externe und interne Kommunikation via Email
-	sicherer Mailversand
-	Umfangreiche und intuitiv anwendbare Termin- und Kalenderfunktionen
-	Professionelle Verwaltung von Kontakten und Gruppen

**Im Rahmen der _openDesk-Suite_ wird die Open-Xchange App Suite mit den folgenden Features eingesetzt:**

-	E-Mail-Anhänge können direkt aus der "Dateien"-App von openDesk übernommen werden
-	E-Mail-Anhänge lassen sich direkt in der "Dateien"-App von openDesk speichern
-	Dateien lassen sich auch als Download-Link in eine E-Mail einfügen. Damit können große oder schützenswerte Dateien ohne Problem versendet werden.
-	Der Zugriff auf Dateien via Download-Link lässt sich zeitlich begrenzen und mit einem Passwort schützen. Das erhöht die Sicherheit zusätzlich.
-	Zu einem Kalender-Eintrag kann direkt ein Link für eine Videokonferenz erstellt werden.

## Zu den funktionalen Highlights gehören

- **Neues Design:** OX App Suite hat eine brandneue Benutzeroberfläche bekommen. Wir haben an unzähligen Details gearbeitet, um Ihnen ein großartiges und zeitgemäßes Nutzungserlebnis bieten zu können.

- **Neue Suche:** Als eines der meistbenutzten Features haben wir die Suchfunktion vereinfacht und nach oben in die Mitte des Navigationsbereichs verschoben. Es ist ein simples und einfach zu benutzendes Eingabefeld mit einem zusätzlichen Dropdown, falls komplexere Suchfilter benötigt werden.

- **Senden zurücknehmen:** "Senden zurücknehmen" ist eine Funktion, mit der Sie das Senden einer E-Mail abbrechen können, kurz nachdem Sie auf die Schaltfläche "Senden" geklickt haben. Diese Funktion ist überaus praktisch, wenn Sie einen Fehler gemacht haben, z. B. vergessen haben, eine Datei anzuhängen oder eine E-Mail an den falschen Empfänger gesendet haben oder einfach aus Versehen auf "Senden" geklickt haben.

- **Dunkelmodus:** Es werde dunkel! Im Dunkelmodus wird statt der üblichen schwarzen Schrift auf weißem Grund, heller Text auf dunklem Grund dargestellt. Neben persönlicher Präferenz hat der Dunkelmodus Vorteile: es wird generell weniger Licht benötigt, was in dunklen Umgebungen die Lesbarkeit verbessert und die Augen entlastet.

- **Themes:** Es werde bunt! Sie mögen Violett? Oder doch lieber Orange? Sie haben die Wahl! Es stehen mehrere Themes mit beliebten Farben zur Auswahl, die Sie mit verschiedenen Hintergründen kombinieren können.

- **Halo View:** Alle Ihre Kontaktinformationen können Sie mit einem einzigen Klick einsehen, genau dann, wenn Sie sie brauchen. Dazu gehört der gemeinsame E-Mail-Verlauf oder auch gemeinsame Kalendertermine.

- **Verwaltete Ressourcen:** Die Funktion "Verwaltete Ressourcen" ermöglicht es Unternehmen, gemeinsam genutzte Ressourcen wie Konferenzräume und Geräte effizient zu verwalten und einzuplanen. Mit dieser Funktion wird sichergestellt, dass die Ressourcen effektiv und effizient genutzt werden, was die Produktivität erhöht und Planungskonflikte reduziert.

- **Meeting-Erinnerung:** Damit Sie den Überblick über Ihren Terminplan behalten, haben wir einen Countdown hinzugefügt, der Sie mit Farbe, Ton und Desktop-Benachrichtigungen an Ihre bevorstehenden Besprechungen erinnert.
