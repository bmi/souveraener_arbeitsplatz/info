**_Die Deutsche Version finden Sie [hier](./OpenProject_as_part_of_openDesk_DE.md)._**

# OpenProject: The Project Management Component for openDesk - What Does the Open Source Software Offer?

**openDesk - The Sovereign Workplace** aims to offer a genuine open source alternative to proprietary vendors, particularly for Public Administration. We would like to keep you up to date on this exciting project.

The individual software components of openDesk cover various areas of work that are used everyday. For example, the open source software OpenProject is used for task and project management. For openDesk, the Berlin-based company is developing project management features that are particularly useful for the public sector. These include, for example, improved meeting documentation, professional PDF export and a high contrast mode for visually impaired users.

This is why we want to take a closer look at OpenProject today: What is possible with the software, both as a standalone version or as a module of openDesk – The Sovereign Workplace?

## Areas of application for OpenProject

**OpenProject** is a project management software that is available under the open source license GNU GPL v3. It is suitable for a wide range of project management approaches, such as waterfall, agile or hybrid, and promotes team efficiency across all phases of a project. This means that organizations of any size and in any industry can use OpenProject for their projects. While some platforms require additional plugins for advanced functionality, OpenProject aims to provide a comprehensive set of features within a single system and along the entire project management life-cycle.

The Community edition is free of charge and available for self-installation. In addition, OpenProject GmbH offers support and extended features for Enterprise customers. Public institutions have been an important user group of OpenProject, and not just since the start of the openDesk project. Special conditions are available for non-profit organizations and educational institutions.

**The use cases of OpenProject are:**

- Project portfolio management
- Scheduling and resource planning
- Task management
- Agile project management, such as Kanban and Scrum
- Requirement management and release planning
- Time and budget tracking
- Joint creation of agendas and meeting minutes

The company puts particular emphasis on aspects such as data protection, security and data sovereignty.

![Screenshot from the OpenProject application](../_media/openproject-screenshot-work-package-gantt-filter.png)

Screenshot from the OpenProject application: Plan your projects and collaborate efficiently with your team.

## Highlight features of OpenProject

- **Work packages:** Create and manage all issues in a project, e.g. tasks, features, risks incl. dependencies, and provides a single source of truth for easier tracking and accountability.

- **Gantt charts:** Collaborative project planning and scheduling.

- **Agile boards:** OpenProject supports agile methodologies such as Scrum and Kanban without the need for additional plugins.

- **Meetings:** Collaborative prepare your meeting agendas and document information by linking agenda items directly to work packages in a project.

- **Baseline comparison:** This feature allows you to compare the current state of a project to previous states, making it easier to track changes and accomplishments.

- **Integrated time and expense tracking:** Unlike many other tools, OpenProject includes both time and expense tracking, which can be crucial for budget management.

- **Role-based access control:** Highly customizable roles and permissions enable clear role definitions and access restrictions.

If you would like to find out more about how OpenProject works and learn more about the features and use cases, take a look at the software's product overview [here](https://www.openproject.org/docs/).