**_Die Deutsche Version finden Sie [hier](./Jitsi_as_part_of_openDesk_DE.md)._**

# Jitsi: The Video Conferencing Component for openDesk - What Does the Open Source Software Offer?

Most video conferencing systems can only be used as a cloud service - on non-GDPR-compliant cloud systems, some of which are covered by the American Cloud Act. It is possible to become independent of third-party manufacturers and operate open-source video conferencing systems "on premise" yourself or have them operated on secure data centers in Germany and the EU.
A good option for this is the open-source video conferencing system Jitsi, which has been developed for many years with a global developer community.

_Jitsi offers an impressive user experience and is quick and easy to use._ Without calendar entries and room assignments, a new video conference room can easily be created "on the fly" via a new URL or the Jitsi start page and the link can then be sent. This means that a new video conference room can be created and shared quickly and spontaneously.

We are part of the developer community and are networked with Jitsi developers worldwide. We offer an Enterprise Service Subscription for Jitsi operators, which includes operational support, further development of the Jitsi modules and feature integration.

## Jitsi OIDC adapter

With the Jitsi OIDC adapter, Jitsi installations can be connected to a Keycloak via OIDC using SSO. Users logged into your environment can open a video conference room with "one click" and forward the link immediately. No scheduling or room booking required - it couldn't be quicker or easier! Jitsi is also connected to the Univention Corporate Server (UCS) via the OIDC adapter and integrated into OpenDesk.

![Screenshot from the Jitsi application](../_media/Jitsi%20english%20and%20deutsch.jpg)
