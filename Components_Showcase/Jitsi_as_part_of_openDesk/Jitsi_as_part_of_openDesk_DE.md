**_Please find the English version [here](./Jitsi_as_part_of_openDesk_EN.md)_.**

# Jitsi: Die Videokonferenz-Komponente für openDesk - Was bietet die Open-Source-Software?

Die meisten Videokonferenzsysteme sind ausschließlich als Cloud Service nutzbar – und zwar auf nicht DSGVO-konformen Cloud-Systemen, von denen wiederum einige dem American Cloud Act unterliegen. Es gibt die Möglichkeit, sich von proprietären Herstellern unabhängig zu machen und Open-Source-Videokonferenzsysteme „on premise“ selber zu betreiben oder auf sicheren Rechenzentren in Deutschland und der EU betreiben zu lassen.

Eine gute Option dafür ist das seit vielen Jahren und mit weltweiter Entwickler-Community entwickelte Open-Source-Videokonferenzsystem Jitsi.

_Jitsi besticht durch die gute User Experience und Einfachheit und Geschwindigkeit in der Bedienung._ Ohne Kalendereinträge und Raumbelegungen kann ganz einfach über eine neue URL oder die Jitsi Startseite „on the fly“ ein neuer Videokonferenzraum erstellt und der Link verschickt werden. So kann auch spontan schnell ein neuer Videokonferenzraum erstellt und geteilt werden.

Wir entwickeln in der Entwickler-Community mit und sind mit Jitsi Entwickler:innen weltweit vernetzt. Wir bieten für Betreiber:innen von Jitsi eine Enterprise Service Subscription an, die Betriebssupport, Weiterentwicklung der Jitsi Module und Feature Integration beinhaltet.

## Jitsi OIDC-Adapter

Mit dem Jitsi OIDC Adapter können Jitsi-Installationen über OIDC per SSO in einen Keycloak angebunden werden. In Ihrer Umgebung eingeloggte User können so per „One Click“ einen Videokonferenzraum öffnen und den Link sofort weiterleiten. Ohne Terminplanung oder Raumbuchung – schneller und einfacher geht’s nicht! Über den OIDC Adapter ist Jitsi auch an den Univention Corporate Server (UCS) angebunden und in OpenDesk integriert.

![Screenshot aus der Jitsi-Anwendung](../_media/Jitsi%20english%20and%20deutsch.jpg)
