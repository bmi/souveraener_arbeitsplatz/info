**_Please find the English version [here](./NeoToolSuite_as_part_of_openDesk_EN.md)_.**

# NeoToolSuite: Die Widgets für Echtzeitkollaboration und -kommunikation für openDesk - Was bietet die Open-Source-Software?

_NeoBoard_ ist ein kollaboratives Whiteboard mit Aufteilung in einzelne Folien gleicher Größe. Es bietet eine gute Team Experience (TX), ein hohes Maß an Barrierefreiheit und effiziente Zusammenarbeit. Durch die Aufteilung in einzelne Folien gleicher Größe entsteht eine bessere Übersichtlichkeit und Handhabung als bei vergleichbaren Whiteboards. Es muss nicht rein- und rausgezoomt werden, und alle haben immer den gemeinsamen Fokus auf dieselben Folien.

**Einsatz-Möglichkeiten:**

-	Visualisierung im Team gemeinsam entwickeln
-	Präsentationen im Team entwickeln, zeigen und bearbeiten
-	Kollaboration durch gemeinsame Arbeit auf den Folien
-	Der Kollaborationsmodus kann individuell pro Folie ein- oder ausgeschaltet werden
-	„Many to Many“ Kommunikation statt „One to Many“ – Präsentieren unter Einbezug der Teilnehmer:innen; So geht Zusammenarbeit und Führung heute
-	Parallele Visualisierung während einer gemeinsamen Videokonferenz
-	Zeitlich asynchrones Bearbeiten von Visualisierungen im Team

Die Folie im NeoBoard ist der zentrale Punkt, von dem aus du mit der (Zusammen-)Arbeit beginnen kannst. Hier kannst du deine Ideen umsetzen, mit anderen Nutzer:innen in den Gedankenaustausch gehen und kreativ werden. Du kannst unbegrenzt viele Folien erstellen und auf jeder Folie unbegrenzt viele Objekte platzieren.

Am unteren Rand jeder Folie findest du die Werkzeugleiste. Damit sind das Hinzufügen und Bearbeiten von verschieden Objekten möglich, mit denen du schnell und einfach die Gestaltung deiner Folie beginnen kannst. "Texte hinzufügen" und "Freihandzeichnen" sind ebenfalls vorhanden.

**Mögliche Live-Meeting-Formate unter Verwendung eines NeoBoard:**

-	Brainstorming
-	Moderationen
-	Trainings / Workshops
-	Meetings
-	Schulstunden
-	Präsentationen
-	Gemeinsame Notizen
-	Diagrammentwicklung

![Screenshot aus der NeoBoard-Anwendung](../_media/NeoBoard01.png)

# NeoDateFix: Souveräne Gestaltung der Kalender- und Meetingstruktur – Was ist das NeoDateFix?

_NeoDateFix_ ist ein Widget für geplante Video-Meetings und der Kern der NeoToolSuite. Es bietet dir die Möglichkeit, terminierte Besprechungen in deinem Element Messenger durchzuführen und alle passenden Widgets einfach einzubinden. Damit hast du eine Lösung zur Hand, die es ermöglicht,

-	eine Jitsi-Videokonferenz mit bis zu 30 Teilnehmer:innen abzuhalten.
-	Abstimmungen mit NeoChoice durchzuführen – entweder personen- oder gruppenbezogen, geplant oder ganz spontan.
-	gemeinsam das NeoBoard zu nutzen – zum Präsentieren und um gemeinsam an Ideen und Projekten zu arbeiten.
-	Notizen mit dem Etherpad anzulegen und mit anderen zu teilen.

All diese Funktionen lassen sich zu deinen Besprechungen hinzufügen und während der Besprechungen nutzen – entweder kombiniert oder einzeln und voneinander losgelöst – so, wie du es brauchst, und natürlich mit der vollen Kontrolle über deine Daten.

**Der Element Messenger bietet dir die Möglichkeiten, dich sowohl**

-	mit einzelnen anderen Nutzer:innen im sogenannten Direkt-Chat auszutauschen, als auch
-	in Räumen mit vielen anderen Nutzer:innen gleichzeitig Unterhaltungen zu führen.

Beide Möglichkeiten sind unkompliziert, komfortabel und zudem sicher für deine Daten. Darüber hinaus sind sie unabhängig von der Zeit: sobald eine Unterhaltung gestartet wurde, kannst du sie so lange fortsetzen und so oft wieder aufnehmen, wie dein:e Gesprächspartner:in ebenfalls „im Raum“ oder „online“ ist. Es gibt keine festgelegte Zeit, zu der die Unterhaltung beginnt und es gibt auch keine Zeit, zu der die Unterhaltung endet.

Plane den Beginn von Besprechungen, entscheide, wie lange sie dauern soll und lege fest, wer daran teilnehmen darf. Mit NeoDateFix kannst du verschiedene Besprechungen individuell und immer wieder anders erstellen und erweiterst so die Möglichkeiten deines Element Messengers.

![Screenshot aus der NeoDateFix-Anwendung](../_media/NeoDateFix01%20deutsch.png)

**NeoDateFix-Features Übersicht:**

-	Einzelne- und wiederkehrende Besprechungen einstellen
-	Serien von Besprechungen mit individuellen Intervallen anlegen
-	Start- und Enddatum und die passende Uhrzeit festlegen
-	Unbegrenzte Anzahl von Teilnehmer:innen einladen
-	Termin Titel- und Beschreibung anlegen
-	Besprechung Löschen bzw. Absagen
-	Zugang zu weiteren Neo-Widgets während des Termins
-	Besprechungskalender in fünf Ansichtsoptionen anzeigen lassen
-	Listenansicht mit chronologisch angeordneten Besprechungen
-	Besprechung auf verschiedene Weise mit anderen Personen teilen: Link zur Besprechung, per E-Mail versenden oder ics-Datei herunterladen, um die Besprechung als Termin in einem Kalender abzulegen

![Screenshot aus der NeoDateFix-Anwendung](../_media/NeoDateFix02%20deutsch.png)

# NeoChoice: Souveräne Team-Abstimmungen organisieren – Was ist NeoChoice?

_NeoChoice_ ist ein Abstimmung-Widget, um schnelle Entscheidungen auf Grundlage individueller Meinungen zu treffen. Mit NeoChoice hast du die Möglichkeit, vielseitige Abstimmungen in vielfältigen Situationen direkt in deinem Element Messenger durchzuführen. Du kannst das Tool nutzen, um einfache, personenbezogene Abstimmungen umzusetzen. Oder du nutzt es, um zusammen mit dem Gruppen-Widget Gruppenzugehörigkeiten (beispielsweise für Schulklassen, Parteien etc.) in der Abstimmung und den Ergebnissen sichtbar zu machen.

**Für die Abstimmung kannst du aus zwei Abstimmungstypen wählen:**

-	die offene Abstimmung
-	die namentliche Abstimmung

Darüber hinaus hast du die Wahl, welchen Antworttyp du für die Abstimmung nutzen möchtest. Es stehen dir die folgenden Optionen zur Verfügung:

-	Ja|Nein
-	Ja|Nein|Enthaltung

Als Moderator:in planst du die Agenda und anstehenden Abstimmungen der Sitzung vorab. Während des Meetings startest du dann die zeitlich begrenzte Live-Abstimmung. Vor dem Starten einer Abstimmung ist es möglich, diese zu bearbeiten oder zu löschen.

![Screenshot aus der NeoChoice-Anwendung](../_media/NeoChoice01%20deutsch.png)

Als Teilnehmer:in einer Abstimmung bekommst du die noch zur Verfügung stehende Zeit für die Abstimmung angezeigt. Die Farben wechseln je nach verbleibender Restzeit:

-	Grün: Es ist noch mehr als die Hälfte der Abstimmungszeit verfügbar.
-	Orange: Es ist weniger als die Hälfte der Abstimmungszeit verfügbar.
-	Rot: Es ist weniger als ein Viertel der Abstimmungszeit verfügbar.

Sobald ein:e Stimmberechtigte:r eine Antwort abgegeben hat, sind die Live-Ergebnisse verfügbar. Die Live-Ergebnisse enthalten ein Diagramm aller Antwortoptionen (Ja|Nein oder Ja|Nein|Enthaltung) sowie eine Legende und – im Fall von namentlichen Abstimmungen – auch eine Liste der Element-Benutzernamen aller Stimmberechtigten, die an der Abstimmung teilgenommen haben. Zusätzlich werden in den Live-Ergebnissen die ungültigen Stimmen angezeigt. Diese kommen zustande, wenn
Stimmberechtigte nicht abstimmen, also von ihrem Stimmrecht keinen Gebrauch machen. Hat also ein:e Stimmberechtigte:r keine Antwort gegeben, wird dies als ungültige Stimme gewertet und im Diagramm entsprechend ausgewiesen.

![Screenshot aus der NeoChoice-Anwendung](../_media/NeoChoice02%20deutsch.png)

**Eine laufende Abstimmung kannst du auf zwei Arten beenden:**

1.	Automatisch durch Ablauf der eingestellten Zeit
2.	Manuell durch den/die Ersteller:in der Abstimmung

Unabhängig von der Art der Abstimmung steht dir nach der Durchführung ein Ergebnisdokument als PDF-Datei zur Verfügung, aus dem die folgenden Informationen hervorgehen:

-	die Teilnehmer:innen
-	das Abstimmungsergebnis (je nach Art der Abstimmung mit und ohne Enthaltung)
-	die Stimmverteilung
-	allgemeine Informationen zur Abstimmung

![Screenshot aus der NeoChoice-Anwendung](../_media/NeoChoice03%20deutsch.png)
