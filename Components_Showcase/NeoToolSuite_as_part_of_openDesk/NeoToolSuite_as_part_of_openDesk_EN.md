**_Die Deutsche Version finden Sie [hier](./NeoToolSuite_as_part_of_openDesk_DE.md)._**

# NeoToolSuite: The Widgets for realtime collaboration and communication for openDesk - What Does the Open Source Software Offer?

_NeoBoard_ is a collaboration board with a fixed board size and split into individual slides. It offers a great team experience (TX), a high degree of accessibility and efficient collaboration. The fixed board size and splitting into individual slides makes it easier to see and use than comparable collaboration boards. There is no need to zoom in and out, and everyone always has the same focus on the same slides.

**Possibilities:**

-	Joint development of visualizations in a team
-	Develop, edit, and present presentations as a team
-	Collaboration through joint work on the slides
-	Each slide is its own space for collaboration
-	Collaboration mode can be switched on or off individually for each slide
-	"Many to Many" communication instead of "One to Many" - presenting with involvement of participants. This is how collaboration and leadership work today
-	Parallel visualization during a joint video conference
-	Asynchronous editing of visualizations in a team

The slide in the NeoBoard is the central point from which you can start working (together). This is where you can implement your ideas, exchange ideas with other users and get creative. You can create an unlimited number of slides and place an unlimited number of objects on each slide.
You will find the toolbar at the bottom of each slide. This allows you to add and edit various objects, which you can use to quickly and easily start designing your slide. Adding text and freehand drawing are also available.

**Possible live meeting formats for using the NeoBoard:**

-	Brainstorming
-	Moderations
-	Trainings / workshops
-	Meetings
-	School lessons
-	Presentations
-	Shared notes
-	Design of diagrams

![Screenshot from the Neoboard application](../_media/Jitsi%20english%20and%20deutsch.jpg)

# NeoDateFix: Sovereign design of the calendar and meeting structures - What is the NeoDateFix?

_NeoDateFix_ is a widget for scheduled video meetings, and it is the center of the NeoToolSuite. It offers you the possibility to hold scheduled meetings in your Element Messenger and to integrate all widgets for this purpose. So you have a solution at hand that allows you to:

-	Host a Jitsi video conference with up to 30 participants
-	Carry out votes with NeoChoice - either person- or group-related, planned or completely spontaneous
-	Use the NeoBoard collaboratively - to present and work together on ideas and projects
-	Create notes with the Etherpad and share them with others
-	All these functions can be added to your meetings and used during the meetings - either combined or individually and separately - just as you need them, and of course with full control over your data

**Element Messenger offers you the opportunity to both:**

-	exchange information with individual users in a direct chat, as well as
-	have conversations in rooms with many other users at the same time.

Both options are uncomplicated, convenient, and secure for your data. On top of that, they are time-independent: once a conversation has started, you can continue it and resume it for as long as your conversation partner is also "in the room" or "online". There is no fixed time for the conversation to start and to end.

Schedule meetings starting time, decide how long they should last and determine who should take part. With NeoDateFix you can do this individually and in different ways for many kinds of meetings and thus expand the possibilities of your Element Messenger.

![Screenshot from the NeoDateFix application](../_media/NeoDateFix01%20english.png)

**NeoDateFix features Overview:**

-	Set individual and recurring meetings
-	Create series of meetings with individual intervals
-	Set a start, end date and time for each meeting
-	Invite an unlimited number of participants
-	Appointment title and description
-	Delete or cancel a meeting
-	Access to other Neo widgets during the appointment
-	Display meeting calendar on five view options
-	List view with chronologically arranged meetings
-	Share meeting with other people in different ways: Link to the meeting, send via email or download ics file to save the meeting as an appointment in your calendar

![Screenshot from the NeoDateFix application](../_media/NeoDateFix02%20english.png)

# NeoChoice: Organize sovereign voting in teams - What is NeoChoice?

_NeoChoice_ is a voting widget for making quick decisions based on individual opinions. With NeoChoice, you can conduct versatile votes in a variety of situations directly in your Element Messenger. You can use it to implement simple, personalized votes. Together with the group widget, you can also use it to make group assignments (e.g. for school classes, parties, etc.) visible during voting and in the results.

**You can choose between two types of voting:**

-	open voting
-	roll-call voting

You also can choose which answer type you would like to use for the vote. The following options are available:

-	Yes|No
-	Yes|No|Abstain

As a moderator, you can plan the agenda items and upcoming votes for the meeting in advance. During the meeting, you then start the timed live vote. Before starting a vote, it is possible to edit or delete the respective poll.

![Screenshot from the NeoChoice application](../_media/NeoChoice01%20english.png)

As a participant in a poll, you will be shown the time still available for voting. The colors change depending on the remaining time:

-	green: More than half of the voting time is still available
-	orange: Less than half of the voting time is available
-	red: Less than a quarter of the voting time is available

As soon as a voter has submitted an answer, the live results are available. The live results contain a diagram of all answer options (Yes|No or yes|no| abstention) as well as a legend and - in the case of roll-call votes - also a list of the element user names of all eligible voters who took part in the vote. There are also invalid votes in the live results. These occur when eligible voters do not vote, i.e. do not make use of their right to vote. If a voter has not given an answer, this is counted as an invalid vote and shown accordingly in the diagram.

![Screenshot from the NeoChoice application](../_media/NeoChoice02%20english.png)

**You can end a voting session in two ways:**

1.	automatically when the set time expires
2.	manually by the creator of the poll

Regardless of the type of voting, a results document is available to you as a PDF file after the voting has been completed, which contains the following information:

-	the participants
-	the voting result (with and without abstentions, depending on the type of vote)
-	the distribution of votes
-	general information about the vote

![Screenshot from the NeoChoice application](../_media/NeoChoice03%20english.png)








