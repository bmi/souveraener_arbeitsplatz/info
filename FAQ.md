**Inhalte / Schnellnavigation**

[[_TOC_]]
# FAQ
## Kategorie: Projekt
### **Was ist das Projekt "openDesk - der Souveräne Arbeitsplatz"?**

Der openDesk ist ein Open-Source (OS)-Entwicklungsprojekt für die Öffentliche Verwaltung, das vom Bundesministerium des Innern und für Heimat (BMI) initiiert wurde. Mit dem Produkt wird eine leistungsstarke OS-Alternative zu proprietären Lösungen im Bereich "digitaler Arbeitsplatz" geschaffen, ausgerichtet am Bedarf der Öffentlichen Verwaltung. Der Fokus liegt auf Digitaler Souveränität, Nutzerfreundlichkeit und Zukunftsfähigkeit. Der openDesk Arbeitsplatz ist browserbasiert und enthält die notwendigen Basisfunktionen zur Unterstützung alltäglicher digitaler Geschäfts- und Arbeitsprozesse sowie virtueller Kollaboration.

Der openDesk Arbeitsplatz nutzt und integriert marktgängige Open-Source-Komponenten, welche im Rahmen des Projektes weiterentwickelt werden. Zudem wird in der IT-Architektur ein konsequenter Fokus auf Modularität, Austauschbarkeit und Interoperabilität gelegt. Neben der Herstellerunabhängigkeit werden dadurch Innovationspotenzial, Sicherheit und Flexibilität in den Fokus gerückt.

### **Wann wird der Quellcode von openDesk zur Verfügung gestellt?**

Der aktuelle Entwicklungsstand (Quellcode) des Souveränen Arbeitsplatzes ist seit Anfang Juli 2023 verfügbar: https://gitlab.opencode.de/bmi/opendesk/component-code

Ende Juli 2023 wurde zudem die Deploymentautomatisierung bereitgestellt, womit eine Referenzinstallation des Souveränen Arbeitsplatzes zur Probenutzung möglich ist.

Im ersten Quartal 2024 soll das erste Release von openDesk für die Öffentliche Verwaltung zur Verfügung stehen. Der aktuelle Arbeitsstand und darauf basierende Preliminary-Releases (Alpha-Stadium) werden zuvor regelmäßig auf Open CoDE veröffentlicht. Dies soll der Transparenz dienen und der Öffentlichkeit einen ersten Eindruck von openDesk vermitteln.

### **Wer steht hinter dem Projekt "openDesk - der souveräne Arbeitsplatz"?**

Das Open-Source-Entwicklungsprojekt ist ein gemeinschaftliches Vorhaben zwischen der Öffentlichen Verwaltung und der privatwirtschaftlichen Open-Source-Community zur Stärkung der Digitalen Souveränität Deutschlands. Der Auftraggeber ist das Bundesministerium des Innern und für Heimat (BMI), vertreten durch die Projektgruppe zum Aufbau des Zentrums für Digitale Souveränität der Öffentlichen Verwaltung (PG ZenDiS).

Die Projekt- und Entwicklungsarbeiten 2023 werden durch den IT-Dienstleister Dataport zusammen mit Open-Source-Herstellern Univention, Open-Xchange, Nextcloud, OpenProject, Collabora, Nordeck, Element und XWiki für die Entwicklung und technische Umsetzung openDesks ausgeführt.

### **Wer finanziert und verantwortet das Projekt "openDesk - der souveräne Arbeitsplatz"?**

Das Projekt wird vom Bundesministerium des Innern und für Heimat (BMI) finanziert. Die Projektgruppe ZenDiS des BMI ist für das Open-Source-Entwicklungsprojekt verantwortlich. Das Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS, www.zendis.de) verantwortet perspektivisch die Weiterentwicklung von openDesk.

### **Warum ist die Entwicklung von openDesk notwendig?**

Mit der zunehmenden Digitalisierung der Öffentlichen Verwaltung steigen die Bedarfe an die Digitale Souveränität. Hohe Abhängigkeiten von proprietären Technologieanbietern können zu Kontrollverlust über die eigene Informationstechnik sowie die Nicht-Einhaltung von nationaler und europäischer Informations- und Datenschutzrichtlinien führen. Die Notwendigkeit der Digitalen Souveränität und der vermehrte Einsatz von Open-Source-Software sind Bestandteil der strategischen Ziele der Bundesregierung und u.a. im Koalitionsvertrag, der Digitalstrategie und der Architekturrichtlinie für die IT des Bundes verankert.

Der digitale Arbeitsplatz openDesk wird zur Mitigation der Abhängigkeiten von proprietären Softwareanbietern sowie zur Sicherstellung der Kontrolle über die eigenen Daten und staatliche Souveränität entwickelt.Zudem befähigt openDesk dessen Nutzende und Betreibende zur flexiblen, skalierbaren, eigenständigen und bedarfsorientierten (Weiter-)Entwicklung der Anwendungen und Funktionalitäten.

### **Worin unterscheiden sich openDesk und die dPhoenixSuite?**

openDesk - Der Souveräne Arbeitsplatz ist vollumfänglich Open Source und integriert
ausgewählte Software-Lösungen zu einer Arbeitsplatz-Suite. Im Vergleich dazu ist die
[dPhoenixSuite](https://www.phoenix-werkstatt.de/) ein Produkt, welches sowohl Open-Source-Lösungen als auch
lizenzierungspflichtige Module bündelt und zusätzliche Service- und Supportleistungen umfasst.

Grundsätzlich soll openDesk mit seiner Architektur und offenen Entwicklung als Standard und
Basis für IT-Dienstleister der europäischen Verwaltung dienen und kann in verschiedenen
Formen, wie bspw. durch das Produkt dPhoenixSuite, ausgestaltet bzw. kommerzialisiert
werden. Als formale Grundlage dient dazu eine Architekturspezifikation, deren aktueller Stand auf Open CoDE veröffentlicht wird.

openDesk baut auf der Idee des Phoenix Projektes auf. Das bedeutet konkret, dass zwar
Aspekte wie u.a. die Architekturideen und die Auswahl der Open-Source-Komponenten als
Grundlage genutzt wurden, aber kein Quellcode übernommen wurde, der nicht vollständig Open
Source ist. Außerdem enthält openDesk zusätzlich neue Open-Source-Lösungen wie u.a. XWiki
und OpenProject.

Ein zentrales Ziel und eine Besonderheit von openDesk ist die zugrundeliegende IT-Architektur
mit konsequentem Fokus auf Modularität, Interoperabilität und Austauschbarkeit der
Komponenten. Somit wird die Digitale Souveränität der öffentlichen Verwaltung gestärkt.
Zusätzlich wird ein Open-Source-Lizenzmodell eingesetzt, das die offene Weiterentwicklung
(auf www.opencode.de) und die Zukunftsfähigkeit des Arbeitsplatzes sicherstellt.


## Kategorie: Open Source und Digitale Souveränität
### **Was ist Open Source?**

Informationen zu dieser Frage und weiteren Fragen rund um "Open Source", sind in den allgemeinen FAQs von Open CoDE zu finden: https://opencode.de/de/faq

### **Inwiefern trägt die openDesk Nutzung zur Stärkung der Digitalen Souveränität bei?**

Das strategische Ziel der Bundesregierung zur Stärkung der Digitalen Souveränität der Öffentlichen Verwaltung wird durch openDesk gestärkt:

1. **Auswahlmöglichkeiten von Softwareprodukten:** Bei der bedarfsgerechten Erweiterung oder Anpassung von openDesk steht ein diversifiziertes Angebot an Open-Source-Softwareprodukten zur Verfügung. Offene Standards gewährleisten Flexibilität und Interoperabilität zwischen Anwendungen. Lock-in-Effekte und Monopolstellungen großer proprietärer Technologieanbieter werden vermieden.
2. **Datenhoheit und -sicherheit durch einsehbare und überprüfbare Quellcodes:** Open-Source-Software Lösungen wie openDesk weisen verhältnismäßig hohe IT-Sicherheitsniveaus auf, da der dahinterliegende Quellcode eingesehen und auf Schwachstellen hin geprüft werden kann. Behördeninterne IT-Sicherheitsbeauftragte können die Quellcodes durch Analysen oder beauftragte Audits schneller prüfen und bewerten. Gleichsam kann die Behebung von Fehlerquellen und Sicherheitslücken durch Schwarmintelligenz und das Mehraugenprinzip auf der Open CoDE Plattform zur schnelleren Identifizierung von Schwachstellen und deren Schließung führen. Durch die Quelloffenheit haben alle die Möglichkeit, die Informationssicherheit von openDesk zu überprüfen und die Berechtigungen zur Nutzung und Verwaltung personenbezogener Daten zu bestimmen. Das Sammeln und Speichern von Metadaten auf außereuropäischen Servern wird vermieden. Sensible Daten können nicht an unbefugte Behörden oder andere Regierungen durchsickern und bleiben in der Hoheit der Bundesrepublik.
3. **Transparente Lizenzbedingungen:** Open Source beruht auf Lizenzmodellen, die Nachnutzenden und Anwendenden der Software das Recht der Einsehbarkeit, Weiterverbreitung und Modifikation der jeweiligen Komponenten einräumt. Die Öffentliche Verwaltung kann dadurch Open-Source-Softwarelösungen wie openDesk für jeden Zweck und ohne Einschränkungen (z.B. Ablauf einer Lizenz, willkürliche geografische Beschränkungen) nutzen.

## Kategorie: Nutzung
### **Welche Vorteile bietet mir openDesk zu anderen Lösungen?**

Der openDesk bietet eine Vielzahl an Vorteilen. Als geplanter Standard für die Öffentliche Verwaltung im Bereich digitaler Arbeitsplatz zeichnet sich openDesk durch Nutzerfreundlichkeit, Sicherheit, Anpassungsfähigkeit und Möglichkeiten der Zusammenarbeit hinsichtlich der Bedarfe der Öffentlichen Verwaltung aus.

Folgende Vorteile ergeben sich durch die openDesk Nutzung:

- **Gesteigerte Produktivität** durch die Verknüpfung einzelner Anwendungen in einer All-in-one-Lösung (inklusive Single-Sign-On über ein zentrales Portal);
- Verstärkte **Kollaboration und Interaktion** mit organisationsinternen und -externen Abteilungen und Externen (aus dem In- und Ausland);
- Breites und wachsendes **Angebot an Komponenten und Funktionalitäten** zur Vereinfachung und Beschleunigung von Geschäfts- und Alltagsprozessen innerhalb der Öffentlichen Verwaltung (z.B. Veraktung, Programm- und Projektmanagement);
- Einhaltung aller grundlegenden **Anforderungen** an eine **sichere und datenschutzkonforme Datenverarbeitung** in der Öffentlichen Verwaltung, wie u.a. Verwaltungsvorschriften, die Architekturrichtlinie des Bundes und die übergreifenden Architekturvorgaben aus den Föderalen Architekturrichtlinien mit ihren Verbindlichkeiten, Standards der Deutschen Verwaltungscloud-Strategie sowie geltende Gesetze (z.B. Datenschutzgrundverordnung, IT-Sicherheitsgesetz);
- **Kompatibilität** zu Dokumenten oder Dateien anderer Arbeitsplatzsoftware;
- **Produktentwicklung** entlang den spezifischen **Bedarfen** von **Bediensteten** der Öffentlichen Verwaltung durch Erprobung mit dem "User Experience Board" (u.a. teilnehmend sind das Robert Koch-Institut, Bundesamt für Seeschifffahrt und Hydrographie);
- **Offenes & flexibles Lizenzmodell**, das Einsehbarkeit, Weiterverbreitung und Modifikation der jeweiligen Komponenten einräumt;
- **Datenhoheit und -sicherheit** durch einsehbare und überprüfbare Quellcodes;
- Flexible Anbindung an und integrierte **Nutzung von Fachverfahren**.

### **Was enthält openDesk an Anwendungen?**

openDesk enthält eine Reihe an funktionalen Anwendungen, die für das digitale Arbeiten in der Öffentlichen Verwaltung benötigt werden. Folgende Übersicht stellt die geplanten Open-Source-Komponenten für die Basisvariante dar:

![Semantic description of image](./Images/openDesk_Komponenten.png)

Die Komponenten sind als integrierte Gesamtlösung über ein zentrales Portal via Single-Sign-on erreichbar. Die openDesk Basisvariante enthält die gängigen Funktionen in den Bereichen Produktivität (z.B. Textverarbeitung), Kollaboration (z.B. gemeinsame Bearbeitung von Dokumenten) und Kommunikation (z.B. Video- und Telefonkonferenzen). In 2023 sind bereits zwei neue Komponenten hinzugekommen um openDesk konsequent weiter am Bedarf der Öffentlichen Verwaltung auszurichten: XWiki zum Wissensmanagement und OpenProject für das Projektmanagement.

Die Komponenten werden entsprechend der Bedarfe der Öffentlichen Verwaltung weiterentwickelt.

### **Basierend auf welchen Rahmenbedingungen wurde openDesk entwickelt?**

Die grundlegenden Rahmenbedingungen in der openDesk Entwicklung umfassen:

- Verwendung von Open-Source-Komponenten und Weiterentwicklung als Open Source,
- Kompatibilität mit dem IT-Grundschutz,
- Sicherstellung der Betreiberunabhängigkeit,
- Bereitstellung über DVS (Deutsche Verwaltungscloud-Strategie),
- Barrierefreiheit,
- offene Standards und Schnittstellen,
- Integration der Komponenten,
- offene und modulare Architektur,
- perspektivische Nutzung auf mobilen Endgeräten,
- perspektivische Offline-Fähigkeit,
- perspektivische VS-NfD-Fähigkeit,
- Webanwendung,
- anpassbares Design,
- einheitliches Look and Feel,
- Single-Sign-On,
- Verfügbarkeit über Open CoDE Repository,
- Sicherheitskonzept (SiKo)-Kompatibilität,
- Dokumentation (Betrieb/Anwendung),
- Skalierbarkeit/Leistung,
- Stand der Technik,
- Einbindung von Nutzern sowie
- eine Betriebsumgebung zum Testen.

### **Wann und wie kann ich openDesk nutzen?**

Aktuell ist openDesk noch in der Entwicklung und nicht für den produktiven Einsatz verfügbar. Im ersten Quartal 2024 wird eine erste Basisvariante veröffentlicht, die einsatzreif ist. Jeder IT-Dienstleister der Öffentlichen Verwaltung kann perspektivisch den Betrieb von openDesk für seine Kundinnen und Kunden übernehmen, da er betreiberunabhängig konzipiert ist.

## Kategorie: Sicherheit & Datenschutz
### **Ist die openDesk Nutzung sicher?**

Die Entwicklung von openDesk findet in Zusammenarbeit mit dem BSI statt und orientiert sich an Sicherheitsstandards der Öffentlichen Verwaltung. Dazu gehören Standards wie der Cloud Computing Compliance Criteria Catalogue, die Einhaltung des IT-Grundschutzes, sowie der Datenschutz nach DSGVO-Vorgaben. Die Open-Source Code der Softwarebestandteile wird regelmäßig aktualisiert und stetig auf Sicherheitslücken überprüft, CVE-Reports und IT-GSC Berichte werden im Laufe des Projektes veröffentlicht. Die openDesk Nutzung stellt also kein höheres Risiko gegenüber proprietären Lösungen dar.

## Kategorie: Entwicklung
### **Wer entwickelt openDesk?**

Die Open-Source-Entwicklung erfolgt primär durch die Open-Source-Hersteller des Projektes: Univention, Open-Xchange, Nextcloud, OpenProject, Collabora, Nordeck, Element und XWiki, dadurch wird sichergestellt, dass die alle Entwicklungen Upstream in die Produkte einfießen und der gesamten Open-Source-Community zur Verfügung stehen; Forks werden so vermieden. Dieser Prozess läuft unabhängig von den Release-Zyklen von openDesk. Der Schwerpunkt liegt auf der aktiven Beteiligung an den Open-Source-Communities, um sicherzustellen, dass Verbesserungen und Änderungen dem breiteren Ökosystem über das Projekt openDesk - der souveräne Arbeitsplatz hinaus zugutekommen. Die Entwicklung findet bei openDesk auf drei Ebenen statt: Anwendungsentwicklung, Integrationsentwicklung sowie Anwendungsintegration und Bereitstellung.

Weiterführende Informationen sind der [OVERVIEW.md](./OVERVIEW.md) zu entnehmen.

Die openDesk Codebase ist auch als Community-Projekt gedacht und soll den Prinzipien der Open-Source-Community gerecht werden. Mit der Veröffentlichung des Quellcodes wird die Entwicklung und Zusammenarbeit mit der Open-Source-Community stärker über Open CoDE forciert. Hierfür stehen auf Open CoDE
- ein [Diskussionsforum](https://discourse.opencode.de/c/projekte/souveraener-arbeitsplatz-projekt-351/700) für allgemeine Themen, Fragen, Feedback, Kritik und Lob, sowie
- ein GitLab [Issue-Board](https://gitlab.opencode.de/bmi/opendesk/info/-/boards) zur Übersicht über Community-Anforderungen und Bug-Meldungen

zur Verfügung.

## **Was wurde bisher entwickelt?**

Im bisherigen Projekt wurden eine Vielzahl an Feature-Erweiterungen und -verbesserungen entwickelt sowie an der verbesserten Integration und Usability der Produkte als Gesamtlösung gearbeitet, der Barrierefreiheit, dem IT-Grundschutz und mehr gearbeitet. Eine weiterführende Übersicht ist in unserer [Roadmap](./ROADMAP.md) zu entnehmen.

## **Ist openDesk ein Fork?**

Nein, openDesk ist die Integration von Open-Source-Produkten zu einer Gesamtlösung für die Öffentliche Verwaltung. Weiterentwicklungen der Komponenten fließen in die Upstream-Repositories der Open-Source-Hersteller ein. Die openDesk Implementation wird als alleinstehende Open-Source-Software auf Open CoDE veröffentlicht.

## **Wie ist openDesk aufgebaut?**

Beim Aufbau von openDesk steht eine souveräne IT-Architektur im Mittelpunkt. Ein konsequenter Fokus auf Modularität, Austauschbarkeit und Interoperabilität sichert dabei die Digitale Souveränität sowie Zukunftsfähigkeit von openDesk.

Die Architektur von openDesk basiert auf einem mit dem Bundesamt für Sicherheit in der Informationstechnik (BSI) und anderen Stakeholdern nach [TOGAF](https://www.opengroup.org/togaf) entwickelten Architekturkonzept. Dieses Konzept gibt das langfristige Entwicklungszielbild für openDesk vor und wird nach der Finalisierung auf Open CoDE veröffentlicht.

Technisch besteht der cloudbasierte openDesk aus Open-Source-Software Komponenten, deren Container in einem Kubernetes Cluster ausgerollt werden. Die Anwendungen sind über ein zentrales Portal und eine einheitliche Navigation innerhalb der Komponenten zugreifbar und befinden sich in einem gemeinsamen Single-Sign-On-Verbund.

## **Wie kann ich an der Weiterentwicklung von openCode mitwirken?**

Wir befinden uns aktuell mitten in der Entwicklung und freuen uns über Verbesserungsvorschläge und Feedback.

Möglichkeiten der Rückmeldung sind im Abschnitt sind unter folgendem Link im Abschnitt "Mitwirkung und Beteiligung" zu finden: https://gitlab.opencode.de/bmi/opendesk/info/-/blob/main/OVERVIEW.md#rückmeldungen-und-beteiligung
