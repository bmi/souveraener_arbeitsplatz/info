<h1>openDesk Release 23.12</h1>

Die Entwicklung von openDesk verzeichnete im Jahr 2023 erhebliche Fortschritte.

Seit Juli 2023 werden laufend aktualisierte [Versionen auf Open CoDE veröffentlicht]((https://gitlab.opencode.de/bmi/opendesk/deployment/sovereign-workplace/-/releases)). Interessierte können diese Versionen installieren und zur Erprobung von openDesk nutzen.

Das Release 23.12 bündelt die gemeinsamen Arbeiten des vergangenen Jahres, ermöglicht mit der dazugehörigen Dokumentation einen tiefen Einblick in den aktuellen Entwicklungsstand von openDesk und bietet erste Ausblicke auf die Weiterentwicklungen im Jahr 2024.

<h2>Inhalte</h2>

* [Zielgruppen-Perspektiven](#zielgruppen-perspektiven)
  * [Nutzer](#nutzer)
  * [Architektur](#architektur)
  * [Betrieb](#betrieb)
  * [Plattform](#plattform)
* [Liefergegenstände](#liefergegenstände)
  * [Architekturkonzept](#architekturkonzept)
  * [Compliance](#compliance)
  * [Datenschutzdokumentation](#datenschutzdokumentation)
  * [Barrierefreiheitsprüfungsergebnis](#barrierefreiheitsprüfungsergebnis)
  * [IT-Grundschutz](#it-grundschutz)
  * [Fachkomponenten](#fachkomponenten)
  * [Plattform](#plattform-1)
    * [Deploymentautomatisierung](#deploymentautomatisierung)
      * [Fachkomponenten](#fachkomponenten-1)
        * [Deployment](#deployment)
        * [Bootstrapping](#bootstrapping)
      * [Servicekomponenten](#servicekomponenten)
      * [Infrastrukturkomponenten](#infrastrukturkomponenten)
    * [Standard CI](#standard-ci)
    * [Tooling](#tooling)

# Zielgruppen-Perspektiven

## Nutzer

Das Nutzererlebnis wurde nicht nur durch eine erste Vereinheitlichung der Oberflächen der einzelnen Fachanwendungen verbessert, sondern auch durch die Vertiefung der Integration zwischen den Anwendungen. In der Zusammenarbeit mit Testnutzenden und Fachleuten für User-Experience hat das Projekt Verbesserungsbedarfe im Bereich der Fachanwendungen identifiziert, welche  mit dem Release 23.12 teilweise behoben werden konnten. Nicht behobene Bedarfe stellen offene Anforderungen für die weitere Entwicklung von openDesk dar.

## Architektur

Das Architekturkonzept definiert das langfristige technische Zielbild für openDesk unter Berücksichtigung der Anforderungen der Deutschen Verwaltungscloud Strategie (DVS). Für die zentralen Bausteine der DVS, wie IT-Grundschutz oder WCAG (Barrierefreiheit) stellen Gap-Analysen wichtige Inhalte für die openDesk-Roadmap 2024 dar.

## Betrieb

Der Betrieb von openDesk in Kubernetes wurde signifikant verbessert. Der IAM-Stack wurde containerisiert, sowie bereits im Juli die containerisierte Open-Xchange Appsuite 8 in openDesk integriert. Bei anderen Anwendungen wurde der Fußabdruck des jeweiligen Containers deutlich reduziert. Die Sicherheitseinstellungen der Anwendungen wurden gehärtet und dokumentiert. Die Sicherheitseinstellungen werden im Jahr 2024, insbesondere auch im Hinblick auf IT-Grundschutz, weiter angepasst.

## Plattform

Im Rahmen der Plattformbereitstellung wurde neben den für die Entwicklung und Evaluation notwendigen Services die Dokumentation weiter ausgebaut. Zudem werden die für das Deployment notwendigen Container-Images und Helm Charts in eigenen Release-Artefakten bereitgestellt, basierend auf dem ebenfalls entwickelten GitLab-CI-Tooling.

Eine der zentralen Aufgaben für 2024 ist es - gemeinsam mit den Open Source Herstellern - einen Useraccount-Lebenszyklus mit bedarfsweiser aktiver Provisionierung abzubilden, der den Anforderungen der öffentlichen Hand gerecht wird.

# Liefergegenstände

Die Liefergegenstände zum Release 23.12 sind, mit entsprechendem git-Tag, auf Open CoDE verfügbar.

## Architekturkonzept

Das Architekturkonzept ist als langfristige Perspektive für openDesk zu verstehen. Es basiert insbesondere auf den Anforderungen aus der [Deutschen Verwaltungscloud Strategie](https://www.cio.bund.de/Webs/CIO/DE/digitale-loesungen/digitale-souveraenitaet/deutsche-verwaltungscloud-strategie/deutsche-verwaltungscloud-strategie-node.html), die auf Standards wie [IT-Grundschutz](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/IT-Grundschutz/it-grundschutz_node.html) und dem [Kriterienkatalog Cloud Computing C5](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Informationen-und-Empfehlungen/Empfehlungen-nach-Angriffszielen/Cloud-Computing/Kriterienkatalog-C5/kriterienkatalog-c5_node.html) fußt. Es wird gemäß [TOGAF](https://www.opengroup.org/togaf) konkretisiert bzw. ausspezifiziert. Die Detailebenen F-H befinden sich aktuell noch in Abstimmung und werden sukzessive mit Fertigstellung ebenfalls veröffentlicht.

Links:
- [Architekturkonzept als GitLab-Pages](https://bmi.usercontent.opencode.de/opendesk-architekturkonzept/A_architekturvision/)
- [Architekturkonzept-Repository](https://gitlab.opencode.de/bmi/opendesk-architekturkonzept)

## Compliance

Die Compliance-Prüfung hat zum Ziel sicherzustellen, dass openDesk ein Open-Source-Angebot ist, welches sich nicht nur an die aus [OSI-Sicht relevanten Open-Source-Lizenzen](https://opensource.org/licenses/) hält, sondern insbesondere auch an die [Liste der auf Open CoDE zulässigen Lizenzen](https://wikijs.opencode.de/de/Hilfestellungen_und_Richtlinien/Lizenzcompliance).

Dazu wurden SBOMs (Software-Bill-of-Materials) im [SPDX](https://spdx.dev/)-Format für die einzelnen Komponenten erstellt. Für Abweichungen vom Zielbild wurde ein Report erstellt, der zugleich auch als Anhaltspunkt dient, an welchen Stellen Optimierungen im Sinne der Lizenzcompliance wünschenswert sind.

Links:
- [Abweichungsreport](https://gitlab.opencode.de/bmi/opendesk/deployment/SBOM/-/blob/23.12/sboms/openDesk%20Deviation%20Report.md)
- [Repository der Lizenz-Compliance Artefakte](https://gitlab.opencode.de/bmi/opendesk/deployment/SBOM)

## Datenschutzdokumentation

Aktualisierung 15.03.2024: Die Datenschutzberichte 23.12 werden vollständig durch die entsprechenden Berichte des Releases 24.03 abgelöst. Siehe [Release Notes openDesk 24.03](../24.03/README.md).

## Barrierefreiheitsprüfungsergebnis

Die Barrierefreiheit von openDesk ist eine wichtige Voraussetzung für den breiten Einsatz - nicht nur - in der öffentlichen Verwaltung. Die erste Barrierefreiheitsprüfung der Anwendungen zeigt auf, dass das Thema Barrierefreiheit grundsätzlich in den Anwendungen Berücksichtigung findet, es jedoch auch Nachbesserungsbedarf gibt. Die Dokumentation des Nachbesserungsbedarfs ist zentraler Bestandteil der Barrierefreiheitsberichte:

- [Admin Portal](./Barrierefreiheit/Prüfbericht%20-%20AdminPortal.pdf)
- [Element & Widgets](./Barrierefreiheit/Prüfbericht%20-%20Kollaborationsmodul.pdf)
- [Nextcloud & Collabora](./Barrierefreiheit/Prüfbericht%20-%20Nextcloud%20und%20Collabora.pdf)
- [Open-Xchange](./Barrierefreiheit/Prüfbericht%20-%20Open-Xchange.pdf)
- [Portal](./Barrierefreiheit/Prüfbericht%20-%20Portal.pdf)
- [XWiki](./Barrierefreiheit/Prüfbericht%20-%20XWiki.pdf)

## IT-Grundschutz

Eines der Ziele von openDesk ist es die IT-Grundschutz-Fähigkeit zu erreichen. Da die Prüfung der Einzelkomponenten, teilweise auf Containerebene, noch nicht vollständig abgeschlossen ist, wird mit 23.12 nur die IT-Grundschutz-Modellierung veröffentlicht. Sie stellt die Einzelkomponenten mit ihrem jeweiligen Prüfumfang dar.

- [Modellierung IT-Grundschutz](./IT-Grundschutz/Modellierung%20-%20Gesamtsicht.pdf)

## Fachkomponenten

Im Rahmen von openDesk wurden diverse Verbesserungen an den einzelnen Open-Source-Komponenten umgesetzt, diese sind im Change-Log aufgeführt.

- [CHANGELOG.md](./CHANGELOG.md)

## Plattform

Alle im folgenden Abschnitt genannten Liefergegenstände werden durch die openDesk Plattform Entwicklung gepflegt und bei Bedarf weiterentwickelt.

### Deploymentautomatisierung

- [Deploymentautomatisierung](https://gitlab.opencode.de/bmi/opendesk/deployment/sovereign-workplace): Dies ist der zentrale Liefergegenstand der Plattform-Entwicklung, aus dem heraus kontinuierlich die [technischen openDesk Releases](https://gitlab.opencode.de/bmi/opendesk/deployment/sovereign-workplace/-/releases) bereitgestellt werden.
- [End-to-End Testsuite](https://gitlab.opencode.de/bmi/opendesk/deployment/e2e-tests): Eine [Playwright](https://playwright.dev/) basierte Ende-zu-Ende Testsuite die mit den dort implementierten Smoke-/Anschalt-Tests das Fundament für den notwendigen weiteren Ausbau der Testautomatisierung bildet.

Die Deploymentautomatisierung nutzt ihrerseits Unterkomponenten, die überwiegend von den Herstellern bereitgestellt werden, aber in Teilen auch aus der Plattform-Entwicklung stammen. Letztere sind nun folgend aufgeführt.

#### Fachkomponenten

Alle entwickelten Helm Charts sollten den [openDesk Best Practises für Helm Charts](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-best-practises) folgen, um möglichst voll qualifizierte Charts bereitzustellen.

##### Deployment

Für einige Fachkomponenten wurden Helm Charts und im Bedarfsfall sogar Images für das Deployment nach Kubernetes erstellt:

- [Dovecot Helm Chart](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-dovecot)
- [Element Helm Chart](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-element)
- [Jitsi Helm Chart](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-jitsi)
- [Matrix Widgets Helm Chart](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-matrix-widgets)
- [Nextcloud Helm Chart](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-nextcloud) und Container-Images:
  - [Nextcloud Base](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-base-layer): Der eigentliche Source Code von Nextcloud mit seinen Apps. Dieser wird im Management Image und im PHP Image verwendet.
  - [Nextcloud Management](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management): Zur Ausführung eines Kubernetes-Jobs zur Verwaltung der Nextcloud-Installation.
  - [Nextcloud PHP](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php): Die PHP Laufzeitumgebung.
  - [Nextcloud Apache2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-apache2): Der Webserver.

##### Bootstrapping

Um Komponenten für den Einsatz in openDesk zu konfigurieren, wurden folgende Bootstrap-Jobs entwickelt:

- [Keycloak Bootstrap Helm Chart](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-keycloak-bootstrap) und [Container-Image](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-keycloak-bootstrap)
- [Open-Xchange Bootstrap Helm Chart](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-open-xchange-bootstrap)
- [OpenProject Bootstrap Helm Chart](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-openproject-bootstrap) und [Container-Image](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-openproject-bootstrap)

#### Servicekomponenten

Servicekomponenten werden über eigens erstellte Helm Charts, und im Bedarfsfall auf Basis eigener Images, deployed:

- [ClamAV ICAP Helm Chart](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav) und [Container-Image](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/clamav-icap)
- [CoTURN Helm Chart](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-coturn)
- [MariaDB Helm Chart](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-mariadb)
- [Postfix Helm Chart](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postfix) und [Container-Image](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/postfix)
- [PostgreSQL Helm Chart](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-postgresql)

#### Infrastrukturkomponenten

Die im Folgenden aufgeführten, infrastrukturbezogenen Komponenten einer openDesk-Instanz erfordern eine zentrale Installation der ebenfalls genannten Basis-Komponenten.

- [Certificates](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates): Deployt auf der Basiskomponente [Cert-Manager](https://cert-manager.io/) basierende `Certificate` Resourcen.
- [openDesk Otterize](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize): Auf Basiskomponente [Otterize](https://docs.otterize.com/) basierendes Deployment von Networkpolicies für die openDesk Plattform. Optional und aktuell standardmässig nicht aktiviert.
- [Istio Gateway](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-istio-resources): Das für die jeweilige Instanz notwendige Gateway der Basiskomponente [Istio](https://istio.io/).

### Standard CI

Eine weitere wesentliche Komponente im Rahmen der Plattform-Entwicklung ist die Standard-CI, welche für GitLab bereitgestellt wird und z.B. bei allen Helm Charts und Container-Images zum Einsatz kommt. Herzstück ist die [gitlab-config](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config): Ein GitLab-CI Framework, um Helm Charts und Container-Images auf Basis von [Semantic Versioning](https://semver.org/lang/de/) zu releasen. Dabei werden u.a. Prüfungsläufe mit verschiedenen Lintern durchgeführt sowie Signaturen erstellt. Weitere Details sind der [Prozess-Beschreibung der Plattform-Entwicklung](https://gitlab.opencode.de/bmi/opendesk/deployment/sovereign-workplace/-/blob/main/docs/workflow.md) zu entnehmen oder natürlich dem zuvor bereits verlinkten gitlab-config Repository selber.

Die CI nutzt folgende für diesen Anwendungszweck erstellte Container-Images:
- [EnvSubst](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/envsubst): Zur Ersetzung von Platzhaltern in Text-Dateien durch Werte aus Environment-Variablen.
- [ClamAV Imagescan](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/clamav-imagescan): Dient dem Malwarescan innerhalb der CI.
- [Helm](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/helm): Ein Image mit den notwendigen Tools wie Helm, Helmfile und Kubectl.
- [Semantic Release](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/semantic-release): Image zur Erzeugung von Semantic Releases, ebenfalls in einer [gepatchten Version](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/semantic-release-patched) verfügbar.

### Tooling

- [Asset-Generator](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-asset-generator): Erzeugt die mit jedem [technischen Release](https://gitlab.opencode.de/bmi/opendesk/deployment/sovereign-workplace/-/releases) bereitgestellten Übersichtslisten der eingesetzten Helm Charts (`charts-index.json`) und Container-Images (`image-index.json`), die z.B. für Deployment-Szenarien genutzt werden können, bei denen alle Artefakte zunächst in einer privaten Registry bereitgestellt werden müssen.
- [gitlab-pull-mirror](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-pull-mirror): Eine Automatisierung zum Spiegeln öffentlicher Git-Repositories in einer konfigurierbaren Gruppenstruktur auf Open CoDE bzw. GitLab.
- [oci-pull-mirror](https://gitlab.opencode.de/bmi/opendesk/tooling/oci-pull-mirror): Eine Automatisierung zur (Teil-)Spiegelung öffentlicher OCI-Registries in die für openDesk passende Gruppen-/Projektstruktur.
- [opendesk-base](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-base): Ein Unterstützungsrepository, welches beispielhaft ein Helmfile Deployment für die im openDesk-Kontext eingesetzten allgemeinen Kubernetes-Komponenten (z.B. Cert-Manager, Istio, CoTURN, Postfix, GitLab-Runner, Kyverno) beinhaltet.
- [renovate-opendesk](https://gitlab.opencode.de/bmi/opendesk/tooling/renovate-opendesk): [Renovate](https://github.com/renovatebot/renovate) basierte Automatisierung für die Komponentenaktualisierung von openDesk.
- [user-import](https://gitlab.opencode.de/bmi/opendesk/tooling/user-import): Ein Tool um zuvor definierte oder zufällige (Demo-)Useraccounts in openDesk anzulegen.
