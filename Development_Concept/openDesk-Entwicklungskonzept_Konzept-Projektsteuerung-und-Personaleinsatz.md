---
pdf_footer: "openDesk Projektsteuerung und Personaleinsatz"
---

## Konzept für die Projektsteuerung und den Personaleinsatz

### Abgrenzung der Projektarbeit vom operativen Tagesgeschäft

Das operative Tagesgeschäft der ZenDiS, des Generalunternehmers als auch der Hersteller umfasst unter anderem die laufenden täglichen Aktivitäten für die Bereitstellung von Enterprise Subskriptionen (z.B. Wartung, Support, Training etc.). 
Dabei sind insbesondere die Entwicklung neuer Software-Releases und die Wartung ihrer Produkte durch die Hersteller ein kontinuierlicher und wiederkehrender Prozess. Eine solche operative Leistungserbringung durch die Hersteller erfolgt primär in permanenten Organisationsstrukturen und nicht in zeitlich befristeten Projektstrukturen.

> [!NOTE]
>
> Wartungs- und Supportleistung erfolgen nicht im Rahmen von Projektstrukturen und müssen beim Hersteller über separate Vereinbarungen beauftragt werden. 

Im Gegensatz dazu werde alle Änderungen ab einem bestimmten Komplexitätsgrad, welche einen hohen finanziellen Aufwand und großen Steuerungsbedarf erfordern, als Projekt durchgeführt. Projekte müssen durch die ZenDiS im Rahmen von Einzelaufträgen vertraglich mandatiert werden und haben einen festen Anfang und ein festes Ende.

> [!IMPORTANT]
>
> Dieses Dokumentes umfasst die Zusammenarbeit der Akteure bei der Weiterentwicklung von openDesk im Rahmen **mehrerer paralleler und temporärer** Teilprojekte. Es handelt sich nicht um ein großes Einzelprojekt. 

Folgende Tabelle gibt einen Überblick über die Abgrenzung der Projektarbeit vom operativen Tagesgeschäft.

| Aktivität                                    | Steuerungsbedarf | Komplexität | Beauftragung                                                 | Projekt |
| -------------------------------------------- | ---------------- | ----------- | ------------------------------------------------------------ | ------- |
| Wartung                                      | gering           | gering      | Subskriptionen bzw. Wartungsvereinbarungen                   | nein    |
| Support                                      | gering           | gering      | Enterprise-Subskriptionen bzw. Support-Vereinbarungen        | nein    |
| Geringfügige Erweiterungen und Optimierungen | gering           | gering      | Einzelaufträge (sowohl T&M-Verträge als auch Gewerke zum Festpreis) | nein    |
| Funktionale Erweiterungen                    | hoch             | hoch        | Einzelaufträge (sowohl T&M-Verträge als auch Gewerke zum Festpreis) | ja      |

### PMflex als Grundlage dieses Projektmanagementsystems

Dieses Konzept für die Projektsteuerung basiert auf dem [PMflex](https://www.bva.bund.de/DE/Services/Behoerden/Beratung/Beratungszentrum/GrossPM/_documents/stda_PMflex.html)-Standard, entwickelt und herausgegeben durch das Kompetenzzentrum (Groß-)Projektmanagement (CC GroßPM) der BVA.

Die Entscheidung zur Wahl dieses Standards basiert auf folgenden Erwägungen:

* Durch die Übernahme des PMflex-Standards können in signifikantem Maße Aufwände für die Entwicklung und/oder Anpassung eigener Standards eingespart werden.
* Bei PMFlex bzw. [PM²](https://pm2.europa.eu/) handelt es sich um eine Methode, in die eine Vielzahl weltweit anerkannter Projektmanagementpraktiken eingeflossen sind.

- Die Methode ist speziell auf die Bedürfnisse der öffentlichen Verwaltung zugeschnitten und hat sich bereits in einer Vielzahl von Projekten der BVA sowie der Europäischen Kommission und anderer europäischer Organisationen bewährt. 

- Im Gegensatz zur proprietären Standards wie Prince2 bzw. PMI wird PM² von der Europäischen Kommission kostenfrei unter einer Open Source Lizenz zur Verfügung gestellt. Das ermöglicht eine uneingeschränkte Anpassung und Anwendung der Methode.

- Die PM²-Methode wird durch das Centre of Excellence in Project Management (CoEPM²) und eine aktive Community-of-Practice kontinuierlich weiterentwickelt und verbreitet. 
- PMflex bzw. PM² bietet ein vollständiges Projektmanagementökosystem an, welches auch Programm- und Portfoliomanagement sowie agile Methoden beinhaltet.

> [!IMPORTANT]
>
> Dieses Dokument konzentriert sich auf die wesentlichen vertragsrelevanten Aspekte. Falls nicht anders im Rahmenvertrag, den Einzelverträgen und diesen Dokumente definiert, gelten die Regelungen des PMflex-Standards.

### Projektorganisation und Rollen

#### Übersicht

In Ergänzung und auch in Abgrenzung zu den bestehenden Aufbauorganisationen des operativen Tagesgeschäfts wird in den openDesk-Projekten der ZenDiS folgende Governance-Struktur definiert:

```mermaid
flowchart TD
  
  PSC -->|beauftragt und koordiniert| SP
  SP-->|beauftragt und koordiniert|SH
   
   subgraph AGB["Angemessenes Entscheidungsgremium/Appropriate Governance Body (ZenDiS GF)"]
   direction TB
    subgraph PSC["Projektlenkungsausschuss (PSC)"]
   	direction LR
   		Endkunde["openDesk Endkunde (Nutzervertreter)"]<-->POT["openDesk Product Owner Team (Projekteigner)"]<-->CB["Community Board (Lösungsanbieter)"]
   end
  end
  
  subgraph SP["Generalunternehmer (tbd)"]
   direction TB
  
        
  end
  
  subgraph SP
   direction LR
      PL[Gesamtprojektleiter]-->PMO[PMO-Team]
      PL[Gesamtprojektleiter]-->TPL["Integrations-Team (Technischer Projektleiter)"]
      PL[Gesamtprojektleiter]-->DSO[DevSecOps-Team]
      PL[Gesamtprojektleiter]-->SAP["Support-Team (Support Projektleiter)"]
      PL[Gesamtprojektleiter]-->QS[QS-Team]
  end
  
  
  subgraph SH["Hersteller der openDesk-Module (Lösungsanbieter)"]
   direction TB
   subgraph Collabora
   	direction TB
		COTPL[Teilprojektleiter]-->COD[Dev.]
		COTPL[Teilprojektleiter]-->COS[Sup.]
        end
    
    subgraph Element
  	 direction TB
		ELTPL[Teilprojektleiter]-->ELD[Dev.]
		ELTPL[Teilprojektleiter]-->ELS[Sup.]
     end

    subgraph Nextcloud
  	 direction TB
		NCTPL[Teilprojektleiter]-->NCD[Dev.]
		NCTPL[Teilprojektleiter]-->NCS[Sup.]
     end

	subgraph Nordeck
  	 direction TB
		NDTPL[Teilprojektleiter]-->NDD[Dev.]
		NDTPL[Teilprojektleiter]-->NDS[Sup.]
     end
	
	subgraph OpenProject
  	 direction TB
		OPTPL[Teilprojektleiter]-->OPD[Dev.]
		OPTPL[Teilprojektleiter]-->OPS[Sup.]
     end

	subgraph Open-Xchange
  	 direction TB
		OXTPL[Teilprojektleiter OX]-->OXD[Dev.]
		OXTPL[Teilprojektleiter]-->OXS[Sup.]
     end

	subgraph Univention
  	 direction TB
		UVTPL[Teilprojektleiter]-->UVD[Dev.]
		UVTPL[Teilprojektleiter]-->UVS[Sup.]
     end
     
	subgraph XWiki
  	 direction TB
		XWTPL[Teilprojektleiter]-->XWD[Dev.]
		XWTPL[Teilprojektleiter]-->XWS[Sup.]
     end

    end

  
```
> [!IMPORTANT]
>
> Je nach Projektziel und -umfang sind nicht immer alle Hersteller an einem Projekt beteiligt.

| Gremium/Rolle                                 | Besetzung                      | Status                                                |
| --------------------------------------------- | ------------------------------ | ----------------------------------------------------- |
| Projektlenkungsausschuss                      | ZenDiS                         | temporär (nur für die Laufzeit eines Einzelprojektes) |
| openDesk Endkunde                             | ZenDiS                         | temporär (nur für die Laufzeit eines Einzelprojektes) |
| openDesk Product Owner Team                   | ZenDiS                         | permanent und projektübergreifend                     |
| openDesk Community Board                      | Generalunternehmer, Hersteller | permanent und projektübergreifend                     |
| Gesamtprojektleiter                           | Generalunternehmer             | permanent und projektübergreifend                     |
| PMO-Team                                      | Generalunternehmer             | permanent und projektübergreifend                     |
| Integrations-Team (Technischer Projektleiter) | Generalunternehmer             | permanent und projektübergreifend                     |
| DevSecOps-Team                                | Generalunternehmer             | permanent und projektübergreifend                     |
| Support-Team                                  | Generalunternehmer             | permanent und projektübergreifend                     |
| QS-Team                                       | Generalunternehmer             | permanent und projektübergreifend                     |
| Teilprojektleiter                             | Hersteller                     | temporär (nur für die Laufzeit eines Projektes)       |
| Hersteller Entwicklungsteams (Dev.)           | Hersteller                     | temporär (nur für die Laufzeit eines Projektes)       |
| Hersteller Support-Teams (Sup.)               | Hersteller                     | permanent und projektübergreifend                     |

#### Projektlenkungsausschuss

Der Projektlenkungsausschuss setzt sich aus folgenden Rollen zusammen:

1. Product Owner Team (Projekteigner)
2. Endkunden (Benutzervertreter)
3. Community Board (Lösungsanbieter)

**Zusammensetzung**: Endkunde der ZenDiS, Vertreter der ZenDiS (Auftraggeberin), Gesamtprojektleiter des Generalunternehmers, Führungskräfte der am Projekt beteiligten Hersteller.

**Verantwortlichkeiten**:

- Genehmigung von strategischen Entscheidungen und Prioritäten im Projekt
- Überwachung des Budgets und der Ressourcenzuweisung
- Konfliktlösung und Eskalationsmanagement
- Überwachung der Zielerreichung und der Erfolgskriterien
- Regelmäßige Berichterstattung an die oberste Führungsebene der ZenDiS

> [!IMPORTANT]
Der Lenkungsausschuss ist für den Erfolg des Projektes verantwortlich.

#### openDesk Endkunde

- **Zusammensetzung**: Führungskräfte, Business Analysten, technische Berater (des Endkunden).
- **Verantwortlichkeiten**:
  - Definition und Priorisierung von Anforderungen und Rahmenbedingungen
  - Definition des Nutzens
  - Abnahme und Validierung der gelieferten Erweiterungen und Ergänzungen

#### openDesk Product Owner Team

Die ZenDiS stellt in ihrer Rolle als Auftraggeberin bzw. Projekteignerin das Product Owner Team. Es wird hierbei beraten durch die Vertreter des Community Boards, insbesondere bei technischen Fragen, welche die Upstream Projekte betreffen.

Das Product Owner Team verantwortet ihre Entscheidungen primär gegenüber der Geschäftsführung der ZenDiS als Angemesssenem Entscheidungsgremium (AGB) und den openDesk Enkunden. Zusätzlich ist es die Aufgabe des Product Owner Teams Synergien bei der Zusammenarbeit mit den Herstellern sicherzustellen.  

- **Zusammensetzung**: Produktmanager, Business Analysten, technische Berater (der ZenDiS).
- **Verantwortlichkeiten**:
  - Definition und Priorisierung von Anforderungen im openDesk Anforderungs Backlogs
  - Abnahme und Validierung der gelieferten Erweiterungen und Ergänzungen
  - Enge Zusammenarbeit mit dem Generalunternehmer und den Herstellern

#### openDesk Community Board

Das Community Board umfasst Vertreter der Hersteller der eingesetzten Open-Source-Anwendungen sowie des Generalunternehmers.

- **Zusammensetzung**: Vertreter der Hersteller und des Generalunternehmers

```mermaid
%%{init: {'theme':'neutral'}}%%
flowchart TD
  
 SP <--> MT
  
subgraph CB[openDesk Community Board]
  	direction LR
  	subgraph SP["Generalunternehmer"]
  	direction TB
  	VSP[Verteter GU]
  	end
   
   subgraph MT["Hersteller der openDesk-Module (Lösungsanbieter)"]
   	direction TB
	VCO[Vertreter CO]
	VEL[Vertreter EL]
	VNC[Vertreter NC]
    VND[Vertreter ND]
	VOP[Vertreter OP]
	VOX[Vertreter OX]
    VUV[Vertreter UV]
    VXW[Vertreter XW]
 	end
end

 
```

- **Verantwortlichkeiten**:
  - Technische Entscheidungen und Richtlinien für die Modul-Integrationen festlegen
  - Überwachung der Architekturrichtlinien und technischen Integrationen
  - Zusammenarbeit mit den Upstream-Projekten zur Sicherstellung der Upstream-Contributions
  - Abstimmung der openDesk-Roadmap mit den Roadmaps der Upstream-Projekte

#### Gesamtprojektleiter

Zur Abwicklung der operativen Projektsteuerung ernennt der Generalunternehmer einen Gesamtprojektleiter. Der Gesamtprojektleiter ist dem Lenkungsausschuss gegenüber berichtspflichtig.

#### Teilprojektleiter

Die an einem Projekt beteiligten Hersteller stellen einen Teilprojektleiter. Auch agile Entwicklungsteams müssen diese Rolle besetzen. 

#### Hersteller der openDesk-Module

Der größte Teil der Entwicklung der openDesk-Module wird auch weiterhin durch die Hersteller der Upstream Projekte finanziert. Auf diese Arbeit setzt die ZenDiS bei der Entwicklung und Bereitstellung von openDesk auf. Die Weiterentwicklung der openDesk-Plattform erfolgt in enger Zusammenarbeit mit den Herstellern der eingesetzten Open Source Anwendungen.    

> [!IMPORTANT]
>
> Zur Sicherstellung von Synergien bei der Entwicklung und Wartung von openDesk erfolgen Entwicklungsaufträge ausschließlich über Upstream Contributions, in Form sog. "*Roadmap Sponsorings*". Bei jeder Weiterentwicklung muss über eine Wartungsvereinbarung die langfristige Wartung dieser Funktionen als Teil der Upstream Projekte sichergestellt sein. 

#### Projektmanagement Office (PMO)

Das PMO unterstützt den Projektleiter bei seinen Projektleitungsaufgaben.

- **Verantwortlichkeiten**: Projektmanagement-Office (PMO) des Generalunternehmers
- **Prozess**:
  - Planung und Überwachung von Projekten
  - Ressourcenplanung und -zuweisung

  - Zeitplanung und Überwachung der Meilensteine
  - Risikomanagement und Eskalation an das Steuerungsgremium

### Projektmanagementsystem

Das Projektmanagementsystem der openDesk-Projekte setzt sich aus den folgend beschriebenen Komponenten zusammen, die gleichzeitig das Regelwerk der Zusammenarbeit in den Projekten beschreiben.  Als Vertragsbestandteil verpflichten sich alle Beteiligten zur Einhaltung dieses Regelwerks.

#### Projektklassifizierung

Diese Klassifizierung hat das Ziel die knappen Ressourcen auf die wesentlichen Aktivitäten zu konzentrieren. Hierzu sieht die PMflex-Projektklassifizierung die Einteilung der openDesk-Projekte je nach Komplexität in die drei Gruppen S, M und L ein. Abhängig von der Projektklasse können einzelne Schritte des Projektmanagements entfallen bzw. sich dessen Umfang und Artefakte maßgeblich reduzieren (siehe PMflex Leitfaden 3 "Anpassung der Methode an das Projekt"). 

Die Klassifizierung erfolgt durch das Product Owner Team während der Initiierungsphase eines Projektes - also noch vor Beginn der Planungsphase.

#### Abgrenzung zu den Projektmanagement-Systemen der Hersteller

Die Hersteller verwenden bei der Entwicklung ihrer Software-Produkte agile bzw. hybride Prozessmodelle. 

> [!IMPORTANT]
>
> Die Entscheidung über die Hersteller-internen Entwicklungsprozesse verbleibt bei den Herstellern.

#### Personaleinsatz und Kapazitätsplanung

Es ist das gemeinsam erklärte Ziel der ZenDiS und des Generalunternehmers, eine effiziente und kontinuierliche Auslastung der beteiligten Teams zu gewährleisten. Hierzu werden gemeinsam mit der ZenDiS geeignete Planungs- und Steuerungsverfahren implementiert, um Lastspitzen und Unterbrechungen zu vermeiden. Die Planungs- und Beauftragungsprozesse werden dahingehend optimiert, dass in allen Phasen der Leistungserbringung eine möglichst gleichmäßige Auslastung erreicht wird. Das Ziel der gleichmäßigen Auslastung und störungsfreien Projektarbeit betrifft den gesamten Projektlebenszyklus. 

Der Generalunternehmer führt einen kontinuierlichen, gemeinsamen Planungsprozess ein, in dem die benötigten Kapazitäten und Fähigkeiten frühzeitig erfasst und als Bedarfe an die Hersteller übermittelt werden. Die Verantwortung für die Ergebnisse dieses Planungsprozess liegt bei der ZenDiS, da nur sie in der Rolle der Projekteignerin die Einzelaufträge mandatieren kann. Es wird eine Vorlaufzeit von zwei bis vier Monaten angestrebt, so dass die Hersteller ausreichend Zeit haben, die erforderlichen Kapazitäten zum Projektbeginn sicherzustellen.

> [!IMPORTANT]
>
> Die Experten für die jeweiligen Module sind auf dem Markt sehr gefragt. Es besteht also ein großes Interesse, dass diese Personen frühzeitig für das openDesk Projekt eingeplant werden können und dann auch nicht wegen Beauftragungslücken dem Projekt verloren gehen.  

Sowohl der Generalunternehmer als auch die Hersteller verpflichten sich ausschließlich Teammitglieder mit ausreichender Qualifikation und Erfahrung in den Projektteams einzusetzen. Hierzu entwickelt die ZenDiS einen verpflichtenden Kriterien- und Anforderungskatalog. Die Prüfung der Qualifikationen und Erfahrung obliegt dem Gesamtprojektleiter des Generalunternehmers. Die getroffenen Entscheidungen werden für den Projektlenkungsausschuss dokumentiert.

Das Onboarding neuer Projektmitglieder erfolgt durch den Generalunternehmer auf Basis eines verpflichtenden Projekthandbuchs. Zusätzlich erfolgt bei Onboarding eine Einweisungen in den Bereichen Datenschutz- und Datensicherheit als auch verpflichtende Schulungen für die eingesetzten Kollaborations- und Kommunikationsprozesse. 

#### Projektbeteiligtenliste

Der Generalunternehmer pflegt eine Liste aller Projektbeteiligte. Diese Liste umfasst mindestens folgende Informationen:

* Rolle und Verantwortlichkeit im Projekt
* E-Mail
* Telefon
* Matrix-ID

Diese Liste ist verknüpft mit dem zentralen Identitäts- und Zugriffsverwaltungssystem. Der Generalunternehmer dokumentiert die datenschutzrechtliche Rechtsgrundlage für die Verarbeitung personenbezogener Daten in den Projekten und stellt auch die Löschfristen sicher.

#### Automation, Standardisierung und Revisionssicherheit

Die Projektmanagement-Prozesse werden nach aktuellem Stand der Technik automatisiert. Hierbei achtet die Gesamtprojektleitung darauf, dass die eingesetzten Verfahren in einem Projekt vereinheitlicht werden und auch verbindlich von allen Projektbeteiligten eingehalten werden.

Die Festlegung der Projektsprache erfolgt im Projektauftrag durch die ZenDiS. Hierbei wird das Ziel des Aufbaus einer aktiven internationalen Entwicklergemeinschaft berücksichtigt. Es sollen Hürden für die Beteiligung von Organisationen aus nicht deutschsprachigen EU-Staaten reduziert werden. Die Vertragssprache ist Deutsch.

Besonderer Bedeutung kommen Artefakte der Beauftragung, Abnahme und Abrechnung, welche zur Sicherstellung der Revisionssicherheit einer strengen Änderungskontrolle unterliegen.  

| Artefakt                                | openDesk-Modul                              | Sprache  |
| --------------------------------------- | ------------------------------------------- | -------- |
| Abnahmeprotokolle                       | OpenProject                                 | Englisch |
| Abrechnungen                            | Nextcloud/Collabora, OpenProject (Workflow) | Deutsch  |
| Arbeitsaufträge                         | OpenProject (Arbeitspakete)                 | Englisch |
| Business Case                           | Nextcloud/Collabora                         | Englisch |
| Dokumentation Best Practices            | XWiki                                       | Englisch |
| Entscheidungsregister                   | OpenProject                                 | Englisch |
| Dokumentation                           | openCode (.md)                              | Englisch |
| Lessons Learned                         | XWiki                                       | Englisch |
| Offene-Punkte-Liste (Issue Log)         | OpenProject                                 | Englisch |
| Projektarbeitsplan                      | OpenProject                                 | Englisch |
| Projektauftrag/Verträge                 | Nextcloud/Collabora, OpenProject (Workflow) | Deutsch  |
| Projekt-Stakeholder-Matrix              | XWiki                                       | Englisch |
| Risikoregister                          | OpenProject                                 | Englisch |
| Tagesordnungen und Sitzungsprotokolle   | OpenProject                                 | Englisch |
| Spezifikationen und technische Konzepte | openCode (.md)                              | Englisch |

Es existiert jeweils nur **eine** Single-Source-of-Truth der Projekt-Artefakte. Durch ein planvolles und strukturiertes Vorgehen sowie durch den Einsatz moderner Tools und Methoden nach dem aktuellen Stand der Technik werden die Qualitäts- und Produktivitätseinbußen vermieden, welche typischerweise durch die Duplikation von Inhalten in Projekten auftreten.   

#### Einsatz der openDesk-Module

Die Zusammenarbeit in den Projektteams erfolgt in einer openDesk-Umgebung, welche durch die ZenDiS bereitgestellt wird. Zur Einbindung weiterer Stakeholder und der Open Source Community wird diese Umgebung mit openCode integriert (SSO, etc.).

> [!IMPORTANT]
Die Nutzung von openDesk in den Projektteams erfüllt eine wichtige Funktion bei der Planung der Entwicklungsschwerpunkte sowie im Rahmen der Nutzeraktzeptanztests.

Der Einsatz der openDesk-Module sowie der ODF-Dateiformate ist verpflichtend - die Nutzung von PowerPoint, Word und Excel in der Projektarbeit ist nur in Einzelfällen zulässig, z.B. bei technischen Abhängigkeiten zu etablierten Fachverfahren der Auftraggeber der ZenDiS.

Die Chat-Kollaboration in den Projektteams erfolgt über das Matrix-Protokoll. Hierbei ist die persönliche Verifikation der Matrix-IDs der Projektteilnehmer verpflichtend (siehe [User Guide](https://static.element.io/pdfs/element-user-guide-german.pdf)).

### Querschnittsprozesse

Folgende Prozesse und Aufgaben umfassen alle Module der openDesk-Plattform:

#### Anforderungsmanagement

- **Verantwortlichkeiten**: Product Owner Team (ZenDiS)
- **Prozess**:
  - Erfassung und Dokumentation von Anforderungen
  - Priorisierung und Einordnung der Anforderungen in das Product Backlog
  - Regelmäßige Review-Meetings zur Aktualisierung und Priorisierung der Anforderungen
- **Strukturplanung/Dekomposition:**

| Objekt  | Dauer              | Roadmap Planung                           | Umgebung                                                   | Verantwortlichkeit           | Entität                      | Beauftragung/Abnahme                                         |
| ------- | ------------------ | ----------------------------------------- | ---------------------------------------------------------- | ---------------------------- | ---------------------------- | ------------------------------------------------------------ |
| Projekt | 1 bis max. 2 Jahre | Jahresplanung                             | [project.opendesk.family](http://project.opendesk.family/) | Product Owner Team           | Projekt in openDesk PM-Modul | Basis für gemeinsame Personalplanung                         |
| Epic    | 1 bis 3 Monate     | Zuordnung zu einem Quartal (kein Release) | [project.opendesk.family](http://project.opendesk.family/) | Product Owner Team           | Arbeitspaket vom Typ Epic    | Definition dedizierter Akzeptanz- und Abnahmekriterien für verbindliche Aufträge und Abrechnung |
| Feature | > Monat            | Ein Release in Upstream Projekt           | Upstream Projekte                                          | Teilprojektleiter Hersteller | Arbeitspaket vom Typ Epic    |                                                              |

#### Integrationsmanagement

- **Verantwortlichkeiten**: Integrations-Team (Generalunternehmer)
- **Prozess**:
  - Entwicklung der beauftragten Erweiterungen und Ergänzungen
  - Integration der Entwicklungen in die Gesamtlösung
  - Durchführung von Tests und Qualitätssicherung
  - Regelmäßige Statusberichte an das Technische Komitee

#### Qualitätssicherung 

Alle im Rahmen der Planung identifizierten Liefergegenstände erhalten eine detaillierte Leistungsbeschreibung, welche eine Qualitätsprüfung und Abnahme ermöglichen. Alle Spezifikationen sind genehmigungspflichtig durch den Lenkungsausschuss und unterliegen der Änderungssteuerung.

- **Verantwortlichkeiten**: QS-Team (Generalunternehmer)
- **Prozess**:
  - Entwicklung der Qualitätsstandards und -richtlinien.
  - Durchführung von Code Reviews und technischen Audits (im Auftrag der ZenDiS).
  - Überwachung der Einhaltung der Qualitätsstandards im (im Auftrag der ZenDiS).

#### DevSecOps

* **Verantwortlichkeiten**: DevSecOps-Team (Generalunternehmer)
* **Prozess**:
  * Aufbau, Wartung und Betrieb der Entwicklungs-, Test- und Staging-Umgebungen des ZenDiS
  * Aufbau, Wartung und Betrieb der CI/CD-Infrastruktur des ZenDiS

### Datensicherheit

Die Übertragung sensitiver Daten erfolgt anhand einer Ende-zu-Ende-Verschlüsselung nach der aktuellen Stand der Technik (siehe auch BSI Leitfaden "[E-Mail-Verschlüsselung in der Praxis](https://www.bsi.bund.de/DE/Themen/Verbraucherinnen-und-Verbraucher/Informationen-und-Empfehlungen/Onlinekommunikation/Verschluesselt-kommunizieren/E-Mail-Verschluesselung/E-Mail-Verschluesselung-in-der-Praxis/e-mail-verschluesselung-in-der-praxis_node.html)". 

Die Verwaltung der Nutzerkonten und Zugriffsrechte erfolgt über ein IDM-System der ZenDiS, welches die zentrale Verwaltung von Nutzerkonten und Zugriffsrechten ermöglicht. Das Teilen sensitiver Projektartefakte über anonyme Freigabelinks ist genehmigungspflichtig durch den Projektlenkungsausschuss.
