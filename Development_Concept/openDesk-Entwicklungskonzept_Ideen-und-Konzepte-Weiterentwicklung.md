---
pdf_footer: "openDesk Ideen und Konzepte zur Weiterentwicklung"
---

# Ideen und Konzepte zur Weiterentwicklung mit detaillierter Beschreibung des Entwicklungspfades

## Überblick

Der vorliegende Abschnitt zielt darauf ab, den Entwicklungsprozess und die Verantwortlichkeiten in openDesk zu erläutern, wobei besonders die Zusammenarbeit zwischen Herstellern, dem Integrationsteam und den Betriebsteams im Fokus steht. 

Anhand von vier sorgfältig ausgewählten Beispielen werden verschiedene Implementierungsszenarien veranschaulicht. Diese Beispiele decken ein breites Spektrum ab: von einfachen Abläufen mit einem Haupthersteller bis hin zu komplexen Prozessen, die mehrere Hersteller und technische Herausforderungen einbeziehen. Zudem werden iterative Prozesse für kontinuierliche Verbesserungen sowie die Integration innovativer Technologien wie von KI-Funktionen dargestellt. 

Die konkreten Beispiele umfassen die Implementierung von @-Erwähnungen in Collabora Online-Kommentaren, die Entwicklung einer globalen Suche im Portal, UI/UX-Verbesserungen und die Integration von KI-Funktionen. Jedes Beispiel bietet eine detaillierte Beschreibung des organisatorischen Prozesses, des Implementierungsablaufs und des Veröffentlichungsprozesses in openDesk, wodurch die vielfältigen Aspekte der Softwareentwicklung und -integration im Projekt anschaulich dargestellt werden.

## Entwicklungsprozess und Verantwortlichkeiten

Die Verantwortlichkeiten im Entwicklungsprozess sind aufgeteilt zwischen den Herstellern, die die Module von openDesk zuliefern, und dem Integrationsteam, das diese zur Gesamtlösung openDesk kombiniert. Die so bereitgestellten Software-Artefakte können dann von Betriebsteams des ZenDiS oder anderer Betreiber übernommen werden.

![Überblick Entwicklungsprozess und Verantwortlichkeiten](./images/openDesk_Development_process_overview.png)

### Akteure und ihre Verantwortlichkeiten

* *Hersteller* implementieren Software, die Grundlage für eines der Module von openDesk ist. Sie stellen über ihre Releaseprozesse sicher, dass die ausgelieferte Software den vereinbarten Anforderungen an das Modul entspricht. Die normale Auslieferungsform auch von im Projekt vereinbarten Weiterentwicklungen sind Standard-Releases der Hersteller mit entsprechender Versionierung, Release Notes und Dokumentation. Nach Absprache werden auch Vorab-Releases von den Herstellern bereitgestellt.
* Das *Integrationsteam* übernimmt die Software-Releases der Hersteller und ist für eine modulübergreifende Qualitätskontrolle verantwortlich, mit Hilfe derer entschieden wird, ob die Module der Hersteller in ihrer Kombination die für das Gesamtprodukt openDesk vereinbarten Anforderungen erfüllen. Das Integrationsteam ergänzt dabei bei Bedarf openDesk-spezifische Konfigurationen und Artefakte, wie beispielsweise das Aktivieren des openDesk-Brandings der Module. Erfolgreich getestete Modul-Updates werden durch das Integrationsteam als Teil eines openDesk-Releases veröffentlicht.
* Die *Betriebsteams*, die im Auftrag des ZenDiS oder anderer Anbieter von openDesk-Deployments agieren, übernehmen das bereitgestellte openDesk-Release in ihre individuellen Betriebsprozesse und stellen laufende Umgebungen für Endanwender-Organisationen zur Verfügung.
* Das *Entscheidungsgremium* beauftragt und koordiniert den Generalunternehmer mit neuen Produktentwicklungen. Dabei validiert und priorisiert es die Anforderungen im openDesk Product Backlog.
* Das *Projektmanagement (PMO)* verantwortet die Planung (Ressourcenplanung und -zuweisung) und Überwachung (Meilensteine) von den aktuellen sowie zukünftigen Projekten. 

Der beschriebene Prozess gilt grundsätzlich für alle Update-Prozesse, also sowohl für die Bereitstellung im Rahmen der Software-Pflege ("Maintenance"), die unabhängig von konkreten Implementierungsbeauftragungen erfolgt, als auch für die Umsetzung von Implementierungen im Rahmen von Arbeitspaketen, da auch die Arbeitspakete vorrangig direkt in die Hauptentwicklung der Software einfließen.

### Liefergegenstände der Hersteller

Die Hersteller stellen Software, deren Quellcode und Dokumentation für Anwender und Betreiber bereit. Die Versionierung der Artefakte und ihre Veröffentlichungszeiträume richten sich nach den Entwicklungsprozessen der Hersteller. Die Hersteller dokumentieren im Rahmen der Bereitstellungen, welche Versionen für den Einsatz in produktiven Umgebungen geeignet und damit von SLAs des Herstellers abgedeckt werden. Teil der Dokumentation für den Betrieb sind Release Notes, in denen Änderungen dokumentiert werden, die für das openDesk-Integrationsteam relevant sind (beispielsweise neue Konfigurationsoptionen).

> [!IMPORTANT]
>
> Die Hersteller sind dafür verantwortlich, Schnittstellen, die der Integration zwischen Software verschiedener Hersteller verwendet werden, stabil zu halten.

### Liefergegenstände und Abläufe des Integrationsteams

Das Integrationsteam übernimmt die Liefergegenstände der Hersteller und führt diese zur Gesamtlösung openDesk zusammen. Dabei erfolgt in einer ersten Stufe ein Abnahmeprozess, der eine neue Version der Software eines Herstellers mit dem ansonsten unveränderten aktuellen Release von openDesk kombiniert und durch möglichst automatisierte Tests die Funktionalität der Gesamtlösung prüft. Bei Projekterweiterungen kann die Abnahme von Lieferungen an diesen Prozess angeschlossen werden. Das Integrationsteam ist dabei für ggf. notwendige Anpassungen von openDesk spezifischen Anteilen an der Gesamtlösung verantwortlich, wie z.B. die Anpassung von automatischen Deployments und Konfiguration neuer Features oder der Gesamtdokumentation.

Ist der Abnahmeprozess eines Moduls, also der Liefergegenstände eines Herstellers, erfolgreich, kann die Aktualisierung Teil des nächsten openDesk-Releases werden und wird entsprechend vorbereitet. Die Entscheidung über Release-Umfänge und -Zeitpunkte liegt beim Product Owner von openDesk. Releases von openDesk werden als gemeinsame Bereitstellung aller Module veröffentlicht.

Die Arbeitsabläufe des Integrationsteams werden vollständig auf openCode abgebildet und alle Arbeitsergebnisse und Releases dort veröffentlicht.

### Vorab-Releases und Integrations-Tests durch Hersteller

Die Hersteller können nach Absprache Vorab-Releases bereit stellen. Diese Releases dienen Test- und Demonstrationszwecken während der laufenden Arbeit an neuen Funktionen und sind daher nicht für den produktiven Einsatz gedacht. Sie können vom Integrationsteam in Testumgebungen eingebunden werden, um gemeinsame Tests und Arbeit an Integrationen zu ermöglichen. 

### Best-Of-Breed-Ansatz in openDesk

Durch die Verwendung von Standardprodukten der verschiedenen Hersteller als Ausgangsbasis für die Module von openDesk besteht die Möglichkeit, von den Herstellern in ihrem normalen, von openDesk unabhängigen Entwicklungsprozess entwickelte, neue Features in openDesk zu integrieren, ohne das eine direkte Anforderung vom Projektlenkungsausschuss besteht. 

Die Hersteller haben können dem Generalunternehmer sowie Projektlenkungsausschuss diese neuen Features vorstellen und bei Wunsch implementieren. Somit entsteht ein Bottom-Up-Prozess, wodurch eine Skalierungsmöglichkeit in der Entwicklung von openDesk entsteht. Der Prozess der Liefergegenstände der Hersteller funktioniert wie oben beschrieben. Besteht Interesse an einem neunen Feature, stellen die Hersteller Software, Quellcode und Dokumentation dem Integrationsteam bereit, welches diese in openDesk zusammenführt. 

## Beispiel 1: Benachrichtigungen und @Erwähnungen in Collabora Online-Kommentaren

### Überblick
Ziel der Implementierung ist die Verbesserung der Kommunikation bei der gemeinsamen Arbeit an einem Dokument. Dazu wird die aus Chat-Systemen bekannte Erwähnung anderer Personen über "@-mentioning" auch in den Kommentaren innerhalb eines Dokumentes in Collabora ermöglicht.

Dieses Beispiel veranschaulicht einen aus Sicht des Projektmanagements unkomplizierten Entwicklungsprozess, an dem hauptsächlich ein Hersteller beteiligt ist.

### Detaillierte Anforderungen:

* Die Zusammenarbeit rund um Dokumente wird erheblich bereichert, indem es einfacher wird, andere interessierte Benutzer eines Dokuments zu benachrichtigen und einzubeziehen. Diese Funktionalität funktioniert gut im Haupttext des Writer-Dokuments, im Kommentarinhalt des Dokuments ist sie jedoch noch nicht vorhanden.
* Durch die Einführung von Benachrichtigungen, die andere Benutzer darüber informieren, dass ihre Eingaben zu einem Dokument innerhalb von Kommentaren angefordert werden.

*Anfängliche Anwendungsfälle ("Umfang" dieser Implementierung):*

* Jeder Benutzer mit Zugriff auf ein Dokument innerhalb von Collabora kann ein @-Zeichen ("@-mentioning") in einem Kommentar verwenden, um eine Erwähnung eines anderen Benutzers zu starten, der über einen Kommentar benachrichtigt werden soll.
* Ein "@-mentioning" löst eine Benachrichtigung der erwähnten Person über den Nextcloud-Benachrichtigungsmechanismus aus.
* Die Funktionalität wird für Textdokumente (Writer), Tabellen (Calc) und Präsentationen (Impress) realisiert.

*Spätere Anwendungsfälle (nicht Teil des anfänglichen Umfangs, aber sollten nicht verhindert werden):*

* Bereitstellung des Benachrichtigungs- und/oder Lesestatus in der Erwähnung.
* Bereitstellung von Avataren im Dropdown-Menü, um Bilder für andere Benutzer anzuzeigen.
* Bereitstellung von Chat-Funktionen im Zusammenhang mit dem Avatar.

### Organisatorischer Prozess

### Entscheidung zur Implementierung:

* Die grundlegende Anforderung, wie oben beschrieben, wird dem openDesk-Entscheidungsgremium vorgestellt, von diesem bewertet und als wertvoll erachtet.
* Die Implementierung als Teil des Upstream-Produkts und die Aufnahme in das Open-Source-Produkt im Hauptzweig sowie die laufende Wartung wurden vom verantwortlichen Hersteller Collabora Productivity bestätigt.

### Mockup-Auswahl und Schätzung

* Collabora erstellt Arbeitspakete mit einer Reihe von Optionen, mit einer ungefähren Schätzung jedes Teils sowie frühen Arbeitspaketen (z. B. Konzeption mit Modellen, Prototypenimplementierung).
* Das Entscheidungsgremium wählt die Optionen aus, die es bestellen möchte, und definiert einen Rahmen für den Aufwand, der es dem Projektmanagement ermöglicht, Entscheidungen zu treffen, ohne das Gremium erneut konsultieren zu müssen.
![Überblick Entwicklungsprozess und Verantwortlichkeiten](./images/cool-comment-wireframes.png)

* Nach Prüfung der Optionen beschließt das Entscheidungsgremium, dass aufgrund der Notwendigkeit einer neuen API für die Erkennung von gesendeten Benachrichtigungen oder für Avatare sowie der hohen Nachfrage nach dieser Funktion ein einfacher Ansatz mit einem einzigen Anbieter für die Implementierung der Kernfunktionalität verfolgt werden sollte. Die Entwicklung der neuen APIs würde separat in einem anderen Arbeitspaket für eine spätere Iteration erfolgen.

### Implementierung

* Collabora übernimmt die Implementierung.
* Da Collabora der einzige beteiligte Hersteller ist, erfolgt die Fortschrittsberichterstattung direkt zwischen dem Projektmanagement und Collabora.
* Das Projektmanagement kann in späteren Sitzungen des openDesk-Entscheidungsgremiums Informationen zum Fortschritt der Implementierung bereitstellen.
* Collabora integriert die Implementierung in seine regulären Veröffentlichungen von Collabora Online Development Edition, die zur Erhebung von umfassendem Community-Feedback und -Test verwendet werden.
* Nach der Akzeptanz im Qualitätsmanagement durch openDesk und der Überprüfung des Community-Feedbacks sowie eventuellen Folgekorrekturen wird sie in eine openDesk-Version integriert.

### Implementierungsprozess

* Durchführung der von Collabora erstellten Arbeitspakete.
* Konzeption mit Entscheidungen zur Softwarearchitektur unter Berücksichtigung langfristiger Anforderungen.
* Implementierung der erweiterten Kommentierungsunterstützung in Kommentierungs-UX-Elementen im Browser.
* Analyse etwaiger Interoperabilitäts- und/oder daraus resultierender Dateiformatprobleme bei der Serialisierung und Speicherung relevanter Daten
* Analyse weiterer API-Anforderungen von anderen Partnern (keine erwartet).
* Implementierung der Barrierefreiheit für das neue Feature mit geeigneten Rollen, zugehörigen Anmerkungen usw.
* Sicherheitsüberprüfung nach der Implementierung, falls erforderlich.
* Prototyp-Benutzeroberfläche, um ein Proof of Concept (PoC) zu ermöglichen und Feedback von Stakeholdern und Testbenutzern zu sammeln.
* Iteration, wo notwendig, um die Funktionalität zu verbessern und zu erweitern, möglicherweise unter Einbeziehung neuer Schätzungen und der Erstellung neuer Folgearbeitspakete
* Finalisierung der Implementierung durch Collabora und Veröffentlichung im unterstützten Collabora Office Upstream.

![Überblick Entwicklungsprozess und Verantwortlichkeiten](./images/cool-comment-result.png)


### openDesk-Veröffentlichung

* Übernahme durch das openDesk-Integrationsteam: QA im Kontext von openDesk, Hinzufügen von Konfiguration und Dokumentation, Veröffentlichung als Teil der openDesk-Veröffentlichung.
* Wenn während der Statusaktualisierungen im Entscheidungsgremium oder während der Qualitätssicherung durch das openDesk-Integrationsteam Probleme auftreten, wird entweder ein Arbeitspaket wieder geöffnet (z.B. nicht erfüllte Akzeptanzkriterien) oder ein neues Arbeitspaket geöffnet (z.B. zusätzlicher Anwendungsfall).
* Veröffentlichungsprozess in openDesk.
* Artefakte als Arbeitsergebnisse sind: Quellcode, Container-Images, SBOM, HELM-Charts, Dokumentation (Betrieb, ggf. Ergänzungen bezüglich BSI-Grundschutz und Architektur).
* Bereitstellung durch den Hersteller Collabora als Teil des Standardprodukts, daher werden die Ergebnisse auf den öffentlichen Ressourcen des Herstellers verfügbar gemacht.
* Das openDesk-Integrationsteam kann auf vorläufige Versionen (Zwischenstufen der Implementierung wie Beta-Veröffentlichungen, Community-Snapshots und/oder Release-Kandidaten) sowie auf vom Hersteller für Produktion und Nutzung freigegebene Versionen für die Gesamtheit der QA zugreifen.
* Nach endgültiger Freigabe durch Collabora wird die endgültige QA vom Integrationsteam durchgeführt. Wenn erfolgreich, werden alle Artefakte für die nächste openDesk-Veröffentlichung übernommen.
* Die Release-Planung für openDesk liegt in der Verantwortung des Product Owners ZenDiS. Die Empfehlung lautet auf eine regelmäßige Veröffentlichung, z.B. monatlich, aller erfolgreich getesteten Updates.

## Beispiel 2: Globale Suche im Portal als Beispiel für einen komplexen Implementierungsablauf

### Überblick

Ziel der Implementierung ist die personalisierte Suche in den Inhalten aller Module von openDesk über einen benutzerfreundlichen Suchdialog im Portal.

Dieses Beispiel veranschaulicht einen komplexen Ablauf bei Projektsteuerung und Entwicklung, da es das organisatorische und fachliche Zusammenspiel vieler Hersteller erfordert.

### Detailliertere Anforderungen

*Anfängliche Anforderungsfälle ("Umfang" dieser Implementierung)*:
- Anforderungsfälle für Endanwender:
    - Endanwender möchten an einer Stelle über alle ihnen in openDesk verfügbaren Informationen (z.B. Dateien, E-Mails, Termine, Chatverläufe) suchen und die Ergebnisse nutzerfreundlich aufbereitet angezeigt bekommen.
    - Endanwender möchten mindestens aus dem Portal heraus eine Suche über alle Module initiieren können.
    - Endanwender möchten in den Suchergebnissen erkennen, um welchen Typ Inhalt (z.B. Dokument oder E-Mail) es sich handelt und eine Vorschau oder einen Ausschnitt des Inhalts im Suchergebnis einsehen können.
    - Endanwender möchten den vollständigen Inhalt eines Suchergebnisses direkt im für diesen Inhaltstyp zuständigen Modul einsehen. Die Suchergebnisansicht lenkt daher beim Öffnen eines Eintrags direkt auf dieses Modul.
- Anforderungsfälle für Betreiber:
    - Die Konfiguration der integrierten Suche ist Teil des Standard-Deployments von openDesk.
    - Ggf. erforderliche zusätzliche Dienste und Datenbanken werden im Demo-Deployment vorkonfiguriert und dokumentiert, ob sie Bestandteil der Produktwartung von openDesk sind.
- Nicht-Funktionale Anforderungen:
    - Die Architektur der Interoperabilitätsschicht wird um standardisierte Schnittstellen und Abläufe zur sicheren Suche über alle Module erweitert, über die per Konfiguration auch zukünftige Module angebunden werden können.
    - Die technische Konzeption stellt sicher, dass Endanwender keine Einträge in den Suchergebnissen vorfinden, auf die sie keinen Zugriff haben (keine Information Leaks).
    - Die Suchergebnisse werden in angemessener Zeit angezeigt, im Regelfall innerhalb von weniger als 5 Sekunden.
    - Die Suchergebnisliste wird dynamisch aus den zurückgelieferten Ergebnissen aufgebaut, sobald ein Modul Rückmeldungen gibt (und z.B. nicht erst, wenn das letzte Modul die Bearbeitung der Anfrage abgeschlossen hat).
    - Die Implementierung kann sinnvoll mit großen Mengen an Suchergebnissen umgehen.
- Sonstige Anforderungen:
    - Im Initialumfang der Umsetzung sind die Module Dokumentenmanagement (Nextcloud), Chat (Element), E-Mail (OX), Kalender (OX), Projektmanagement (OpenProject), Wissensmanagement (XWiki) und Portal (Univention). Suchergebnisse werden aus Inhalten aller Module gebildet.

*Spätere Anforderungsfälle (nicht Teil des initialen Umfangs, aber sollten nicht verhindert werden):*
- Use Cases für Endanwender:
    - Endanwender möchten Suchergebnisse nach Kriterien filtern können: durch komplexe Suchanfragen (durch Verknüpfung von Suchanfragen wie "UND" und "ODER").
    - Endanwender möchten die gleiche Suchmaske in allen Modulen verwenden können, dabei ist ein Fokus auf Suchergebnisse aus dem aktuellen Modul-Kontext wünschenswert.

### Organisatorischer Prozess

Die Implementierung benötigt eine Koordination fast aller Akteure im Projekt und betrifft mehrere technische Ebenen (Frontend und Backend). Aufgrund dieser Komplexität bestehen Risiken, die die Projektorganisation adressieren muss. Die Umsetzung wird dazu in mehrere aufeinander aufbauende Arbeitspakete aufgeteilt, die jeweils im Steuerungsgremium bewertet werden, bevor die nächste Phase zur Umsetzung freigegeben wird.
Es gibt einen führenden Hersteller für die Implementierung, der die Koordination der Arbeiten in diesen Arbeitspaketen übernimmt.

Es gibt folgende Phasen in der Umsetzung:

1. Phase - Konzeption und Aufwandschätzung je Modul
    - Ziele: Konzeption der Software-Architektur mit Auswahl der Datenmodelle, Schnittstellen und Standards unter Benennung der technischen Risiken, UI-Konzeption (Mockup) mit Feedback-Loop der Stakeholder, grobe Aufwandschätzungen je Modul für die folgenden Arbeitspakete.
2. Phase - "Proof of Concept" (PoC)
    - Ziel: PoC mit klickbarem UI-Prototyp und mindestens zwei angebundenen Modulen, bei dem die technischen Risiken geklärt oder bewertet wurden, aktualisierte und belastbare Aufwandsschätzungen für das folgende Arbeitspaket.
3. Phase - Implementierung
    - Ziel: Schrittweise Umsetzung in allen Modulen
    - Vorgehen wird aufgeteilt: Initial werden Portal sowie 1-2 Module aus dem PoC umgesetzt, danach die weiteren Module angebunden.
    - Es gibt einen führenden Hersteller oder das Projektmanagement (Niels, übergeben wir die gesamte Koordination dem Projektmanagement?) für die Implementierung, der die Koordination der Arbeiten in diesem Arbeitspaket übernimmt.
    - Bei Bedarf (z.B. zu großes Budget) wird in weitere Arbeitspakete aufgeteilt.

### Implementierungsablauf

1. Phase - Konzeption und Aufwandschätzung je Modul
    - Für das Arbeitspaket wird ein Architekten-Team aus mindestens zwei Herstellern gebildet sowie ein Technisches Komitee mit Beteiligungsmöglichkeit aller später involvierten Hersteller und Vertreter des Auftraggebers. Zwischenergebnisse und Designentscheidungen werden vom Architekten-Team erarbeitet und im Technischen Komitee bewertet und verabschiedet.
    - Die Umsetzung erfolgt nach Best Practices bei der Lösungskonzeption: Die Anforderungen werden ausformuliert und bewertet, verfügbare offene Standards und Implementierungen geprüft und nichtfunktionale Anforderungen wie die Einhaltung von BSI-Grundschutz und modulare Integration auch für zukünftige Änderungen an den Modulen über einer Interoperabilitätsschicht einbezogen, um eine Lösung auszuwählen, die in einem Konzept beschrieben wird. Bei Bedarf werden technische "Spikes" zur Absicherung des Konzepts durchgeführt.
    - Die Ergebnisse (Konzepte) werden dem Auftraggeber übergeben und können z.B. auf openCode veröffentlicht werden.
2. Phase - PoC
    - Die Implementierungen erfolgen in Feature-Branches der Hersteller, aus denen Testversionen von Sourcecode und Artefakten entstehen. Diese Ergebnisse stehen dem Auftraggeber zur Verfügung, werden aber von den Herstellern nicht zur produktiven Nutzung freigegeben.
    - Die gemeinsamen Tests erfolgen in einer Testinstanz von openDesk, die durch das Integrationsteam bereitgestellt wird. Die Testinstanz bezieht ihre Artefakte aus den Feature-Branches der Hersteller.
    - Das Konzept aus Arbeitspaket 1 wird nach Bedarf aktualisiert, z.B. wenn technische Risiken geklärt werden konnten.
    - Die Testinstanz ist Abnahmeumgebung für das Arbeitspaket.
3. Phase - Implementierung
    - Die Implementierung wird in die herstellerspezifischen Arbeiten aufgeteilt.
    - Die Umsetzung erfolgt schrittweise, beginnend mit notwendigen Erweiterungen am Portal als zentraler Suchmaske und ggf. Backend-Services der Interoperabilitätsschicht.
    - Die Umsetzung innerhalb der Module, z.B. durch Bereitstellung von Schnittstellen zur Interoperabilitätsschicht, erfolgt je Hersteller.
    - Um unnötige Abhängigkeiten bei Software-Releases zu vermeiden, werden alle Erweiterungen so umgesetzt, dass sie deaktivierbar sind oder es werden anderweitige Maßnahmen ergriffen, die die Bereitstellung von Updates sicherstellen.
    - Als Teil der Implementierung erfolgen gemeinsame Integrationstests auf einer vom Integrationsteam bereitgestellten Testumgebung.
    - Das Integrationsteam erweitert die openDesk-spezifischen Testfälle.
    - Die Auslieferung als openDesk Release erfolgt schrittweise, um möglichst früh Erfahrungen mit Suchergebnissen für erste Module in produktiven Umgebungen zu sammeln. Dabei werden Abhängigkeitsketten beachtet.

Als neues Feature kann eine Weiterentwicklung auf Basis von Endanwenderfeedback sinnvoll sein. Dazu sollte die Arbeit über ein iteratives Arbeitspaket erfolgen und kann Teil der Betrachtungen im Beispiel 3 (Usability) in diesem Dokument sein.


### Veröffentlichungsprozess in openDesk

- In Phase 1 und 2 kann eine Veröffentlichung von Konzepten und PoCs auf openCode erfolgen, um den Arbeitsstand transparent zu halten. Releases sind hier nicht vorgesehen, daher sollten diese Veröffentlichungen auf openCode auch in Feature-Branches erfolgen.
- Die Ergebnisse von Phase 3 und folgende werden als Release-Updates durch die Hersteller veröffentlicht.
- Das Integrationsteam wird von den Herstellern über die Produktdokumentation und Projektkommunikation informiert, ab welchem Release die Funktion verfügbar ist, und kann sie darauf aufbauend im Rahmen von openDesk Releases schrittweise aktivieren.


## Beispiel 3: Verbesserungen an UI/UX Elementen als Beispiel für einen iterativen Implementierungsablauf

### Überblick

Ziel der Implementierung ist die Verbesserung der Einheitlichkeit und Benutzerfreundlichkeit der Benutzeroberfläche in Reaktion auf Rückmeldungen von Reviews, Auftraggebern, Testern und Kunden. 
Dieses Beispiel steht für einen iterativen Ablauf in einem Arbeitsfeld, das eine stetige Weiterentwicklung erfordert. Die Änderungen erfordern keine Anpassungen an der Softwarearchitektur, daher ist kein Technical Committee notwendig.

### Detailliertere Anforderung

- Die Anforderungen werden als Problemstellung formuliert und basieren idealerweise auf echtem Anwenderfeedback, z.B. "75% der Probanden in einem Usability-Test finden eine Funktion nicht" oder "20% der produktiven Kunden berichten von wiederkehrenden Supportfragen zu einer Funktion."
- Es werden keine vorgegebenen Lösungen vom Auftraggeber als Anforderungen formuliert, da die Lösungsfindung von den Herstellern passend zum Produkt erfolgen sollte.
- Es ist erkennbar, dass eine Umsetzung keine Änderungen an der Softwarearchitektur oder Schnittstellen benötigt und nicht die Funktionalität der Software ändert. Sie erfüllt damit alle Kriterien, um als Update einer Software-Hauptversion (also ohne "breaking changes", die einen Versionssprung erfordern) ausgeliefert zu werden.

### Organisatorischer Ablauf

- Für Verbesserungen im Bereich Usability wurde vom Lenkungsausschuss ein Budgetrahmen definiert und eine Budget-Obergrenze für einzelne Umsetzungen festgelegt. Innerhalb dieses Rahmens erfolgen Entscheidungen im Ermessen des Projektmanagements und einer Ansprechperson des Auftraggebers, die gegenüber dem Lenkungsausschuss berichten.
- Aus den Problemstellungen wird ein priorisiertes Backlog erstellt, das schrittweise abgearbeitet wird.
  
### Implementierungsablauf

- Implementierungen werden bei Bedarf in Einheiten je Hersteller bzw. Integrationsteam aufgeteilt.
- Der Hersteller erstellt nach Bedarf Konzepte/Mockups für eine Verbesserung im Produkt, um Feedback vom Auftraggeber oder Endanwender einzuholen.
- Die Umsetzung erfolgt durch den Hersteller, ggf. in mehreren Teillieferungen, als Teil des Update-Prozesses der in openDesk eingesetzten Produktversion.

### Veröffentlichungsprozess in openDesk

- Die Bereitstellung erfolgt als inkrementelles Update der eingesetzten Softwareversion und folgt dem Bereitstellungsprozess für Sicherheitsupdates.
- Es werden die bestehenden Testabläufe bei Hersteller und im openDesk Integrationsteam durchlaufen.


## Beispiel 4: KI Funktionen für openDesk

### Überblick

Ziel der Implementierung ist die Integration von KI-gestützten Komponenten, um die Effizienz der Nutzung von openDesk zu erhöhen, durch intelligente Assistenten und Suchfunktionen, automatische Verschlagwortung und Dokumentenverarbeitung sowie Empfehlungen.

### Detailliertere Anforderungen

*Anfängliche Anforderungsfälle (Im Umfang sind verschiedene mögliche Anforderungen beschrieben. Diese werden bewusst angegeben, da noch nicht hinreichend beobachtet wurde, welcher Anforderungsfall den größten Kundennutzen bietet)*

Künstliche Intelligenz bietet viele Anforderungsfälle, welche in der zukünftigen Produktentwicklung eine wichtige Rolle spielen werden. Im Folgenden sind nur ein paar mögliche KI-Funktionen beschrieben, welche in openDesk integriert werden könnten.
- Intelligente Assistenten:
    - Integration von KI-basierten Assistenten für verschiedene Anwendungen, z.B. für Textverarbeitung, E-Mail-Management oder Terminplanung.
    - Diese könnten Aufgaben wie Textzusammenfassungen, Vorschläge für E-Mail-Antworten oder intelligente Terminvorschläge übernehmen.
- Automatisierte Dokumentenverarbeitung:
    - KI-gestützte Analyse und Kategorisierung von Dokumenten.
    - Automatische Extraktion wichtiger Informationen aus eingehenden Dokumenten.
- Verbessertes Wissensmanagement:
    - KI-basierte Suchfunktionen, die kontextbezogene und semantische Suchen ermöglichen.
    - Automatische Verlinkung verwandter Dokumente und Informationen.
- Predictive Analytics:
    - Vorhersagemodelle für Arbeitslasten oder Ressourcenbedarfe.
    - Frühwarnsysteme für potenzielle Probleme oder Engpässe in Verwaltungsprozessen.
- Chatbots für interne Unterstützung:
    - KI-gesteuerte Chatbots zur Beantwortung häufiger Fragen zu IT-Systemen oder Verwaltungsprozessen.

### Organisatorischer Ablauf

- Für die Einführung von künstlicher Intelligenz wurde vom Lenkungsausschuss ein Budgetrahmen definiert und eine Budget-Obergrenze für einzelne Umsetzungen festgelegt. Innerhalb dieses Rahmens erfolgen Entscheidungen im Ermessen des Projektmanagements und einer Ansprechperson des Auftraggebers, die gegenüber dem Lenkungsausschuss berichten.
- Die Anforderungen/Probleme der Nutzer werden erhoben, in einem Backlog priorisiert und implementiert.

### Implementierungsablauf

Phase 1: Konzeption und Aufwandschätzung je Modul
- Anforderungserhebung durch Workshops mit Endnutzern und Stakeholdern.
- Technologische Machbarkeitsstudie zur Integration von KI-Technologien.
- Erstellung eines Pflichtenheftes mit detaillierten Anforderungen.
- Alternativ die Definition eines Zielraumes für die Entwicklung in einem agilen Prozess, sofern die Anforderungen nicht detailliert spezifizierbar sind. Dies trifft insbesondere für Features mit "research & development" Character zu. Checkpoints zur Konkretisierung des Ziels mit dem Entscheidungsgremium und PO Team wie z.B. reviews innerhalb des Entwicklungsprozess sind individuell je nach Character des zu entwickelnden Features zu definieren.

Phase 2: PoC (Iterativer Loop: Phase 2 kann im Sinne agiler Vorgehensweise mehrfach durchlaufen werden.)
- Entwicklung eines Prototyps mit grundlegenden Funktionen wie intelligenten Suchalgorithmen und automatischer Verschlagwortung.
- Durchführung von Usability-Tests und Feedback-Sessions mit ausgewählten Nutzern.
- Durchführung von Reviews mit Entscheidungsgremium und PO Team

Phase 3: Implementierung des Prototyps
- Erweiterung des Prototyps um zusätzliche Funktionen wie Dokumentenempfehlungen und kontextbasierte Hilfe.
- Integration der KI-Komponente in die bestehende Umgebung von openDesk.
- Durchführung von umfassenden Tests und Feinabstimmungen basierend auf Nutzerfeedback.

Phase 4: Implementierung
- Ausrollen der neuen Komponente an alle Nutzer.
- Fortlaufende Unterstützung und Anpassungen basierend auf Echtzeit-Feedback.

Als neues Feature kann eine Weiterentwicklung auf Basis von Endanwenderfeedback (Teil von Phase 4) sinnvoll sein. Dazu sollte die Arbeit über ein Iteratives Arbeitspaket erfolgen und kann Teil der Betrachtungen im Beispiel 3 (Usability) in diesem Dokument sein.

### Veröffentlichungsprozess in openDesk

- In Phase 1 und 2 kann eine Veröffentlichung von Konzepten und PoCs auf openCode erfolgen, um den Arbeitsstand transparent zu halten. Releases sind hier nicht vorgesehen, daher sollten diese Veröffentlichungen auf openCode auch in Feature-Branches erfolgen.
- Die Ergebnisse von Phase 3 und 4 werden als Release-Updates durch die Hersteller veröffentlicht.
- Das Integrationsteam wird von den Herstellern über die Produktdokumentation und Projektkommunikation informiert, ab welchem Release die Funktion verfügbar ist, und kann sie darauf aufbauend im Rahmen von openDesk Releases schrittweise aktivieren.
