<h1>openDesk Überblick</h1>

**Inhalte / Schnellnavigation**
* [openDesk Elevator Pitch](#opendesk-elevator-pitch)
* [Disclaimer](#disclaimer)
* [Inhalt und Zielgruppe dieses Dokuments](#inhalt-und-zielgruppe-dieses-dokuments)
* [Entwicklung von openDesk](#entwicklung-von-opendesk)
  * [Anwendungsentwicklung](#anwendungsentwicklung)
  * [Integrationsentwicklung](#integrationsentwicklung)
  * [Anwendungsintegration- und Bereitstellung](#anwendungsintegration--und-bereitstellung)
* [Komponenten von openDesk](#komponenten-von-opendesk)
  * [Helm Charts und Container-Images](#helm-charts-und-container-images)
  * [Weitere Liefergegenstände](#weitere-liefergegenstände)
* [Releases](#releases)
* [Rückmeldungen und Beteiligung](#rückmeldungen-und-beteiligung)
  * [Rückmeldungen](#rückmeldungen)
  * [Beteiligung](#beteiligung)
    * [Anwendungs- und Integrationsentwicklung](#anwendungs--und-integrationsentwicklung)
    * [Anwendungsintegration- und Bereitstellung](#anwendungsintegration--und-bereitstellung-1)
* [Fußnoten](#fußnoten)

# openDesk Elevator Pitch

openDesk integriert Open Source Anwendungen bekannter Anbieter zu einer browserbasierten Open Source Plattform für Zusammenarbeit.

**openDesk ist ein digitaler Arbeitsplatz für die Öffentliche Verwaltung mit Fokus auf Digitale Souveränität, Nutzerfreundlichkeit und Zukunftsfähigkeit.**

Das Open Source Softwareprodukt "openDesk" ermöglicht die Wiederverwendbarkeit von Open Source Quellcodes der Öffentlichen Verwaltung und gibt Raum zur Teilhabe an der Weiterentwicklung. Flexible Weiterentwicklungsmöglichkeiten erlauben das Einbringen eigener Ideen, Anforderungen und Anwendungen.

Als Betriebsumgebung von openDesk kommt Kubernetes zum Einsatz. Die teilweise nicht originär für den Containerbetrieb ausgelegten Anwendungen werden dabei mehr und mehr für dieses Betriebsszenario optimiert.

# Disclaimer

Es gelten die Inhalte der Seite [Häufig gestellte Fragen von Open CoDE](https://opencode.de/de/faq).

# Inhalt und Zielgruppe dieses Dokuments

Dieses Dokument gibt einen Überblick über openDesk auf https://gitlab.opencode.de/bmi/opendesk und hat daher eine technische Ausrichtung.

Lesen sollte es wer
- daran interessiert ist, aus welchen Bestandteilen openDesk besteht und wie dieser aufgebaut ist,
- wissen möchte, welche Inhalte zu openDesk im GitLab auf Open CoDE zu finden sind und/oder
- wie eine Mitwirkung an diesem Projekt erfolgen kann.

# Entwicklung von openDesk

Entwicklung findet bei openDesk auf drei Ebenen statt:

1. **Anwendungsentwicklung**: Weiterentwicklung der eingesetzten Open Source Kernkomponenten durch den jeweiligen Hersteller, um neue Funktionen zur Verfügung zu stellen und den Betrieb in Kubernetes zu verbessern.
2. **Integrationsentwicklung**: Entwicklung von openDesk-spezifischen Integrations- und/oder Brandingpaketen für die Kernkomponenten durch den jeweiligen Anbieter.
3. **Anwendungsintegration und Bereitstellung**: Entwicklung / Erstellung von Arbeitsergebnissen die spezifisch für openDesk sind, wie Deployment- und Testautomatisierung, sowie SBOMs, Anleitungen und Prüfberichte.

## Anwendungsentwicklung

**Anmerkung:** Wenn in diesem Dokument von "Community-Version" gesprochen wird, dient dies zur Abgrenzung von der Variante des jeweiligen Produktes, welche im Regelfall über Subskriptionen kommerziellen Support erfährt und teilweise auch über einen erweiterten Funktionsumfang verfügt. Diese mit Zahlungen verbundene Version wird im Kontext dieses Dokuments wiederum pauschal als "Enterprise Versionen" bezeichnet. Das Wording der einzelnen Hersteller kann von der Nomenklatur "Community" und "Enterprise" jedoch abweichen.

Der openDesk nutzt die Community-Versionen der jeweiligen Produkte und bietet damit die Möglichkeit, zu Test- und Analysezwecken ein funktionsfähiges Deployment durchzuführen (Referenzdeployment).

Der Sourcecode der Anwendungen wird auf Open CoDE zur Verfügung gestellt. Die Links in der unten stehenden Liste verweisen auf die entsprechenden GitLab Gruppen auf Open CoDE, unterhalb derer der Sourcecode zu finden ist. In der jeweiligen Repository-Beschreibung sind Informationen über die Herkunft des Sourcecodes zu finden. Die meisten Repositories werden über einen Pull-Mirror[^1] von Github aktuell gehalten.

Den funktionalen Kern von openDesk bilden die im folgenden genannten Anwendungen der ebenfalls benannten Hersteller:

- Die Online-Office-Lösung [Collabora](https://gitlab.opencode.de/bmi/opendesk/component-code/office/collabora) von [Collabora Ltd.](https://www.collabora.com/)
- Die Messenger-Lösung [Element](https://gitlab.opencode.de/bmi/opendesk/component-code/realtimecommunication/element) der [Element GmbH](https://element.io)
- Die Videokonferenz-Lösung [Jitsi](https://gitlab.opencode.de/bmi/sopendesk/component-code/realtimecommunication/nordeck) bereitgestellt durch die [Nordeck IT + Consulting GmbH](https://nordeck.net/)
- Die Dateisynchronisations und -zusammenarbeits-Lösung [Nextcloud](https://gitlab.opencode.de/bmi/opendesk/component-code/file/nextcloud) der [Nextcloud GmbH](https://nextcloud.com/de/)
- Die Groupware-OX-AppSuite 8 (*Todo: Link einfügen wenn OX AppSuite 8 Code verfügbar*) mit dem zugehörigen IMAP-Mail-Backend [OX Dovecot](https://gitlab.opencode.de/bmi/opendesk/component-code/groupware/dovecot) der [Open-Xchange AG](https://www.open-xchange.com/)
- Die Projektmanagement-Software [OpenProject](https://gitlab.opencode.de/bmi/opendesk/component-code/project-management/openproject) der [OpenProject GmbH](https://www.openproject.org/)
- Die Identity and Access Management Plattform [Univention Corporate Server](https://gitlab.opencode.de/bmi/opendesk/component-code/crossfunctional/ucs) der [Univention GmbH](https://www.univention.de/)
- Die Wissensmanagement-Lösung [XWiki](https://gitlab.opencode.de/bmi/opendesk/component-code/knowledge-management/xwiki) und die sichere Bearbeitungsplattform CryptPad (*Todo: Link einfügen wenn CryptPad in openDesk integriert ist*) von der [XWiki SAS](https://www.xwiki.org/)

## Integrationsentwicklung

Einige Hersteller entwickeln Integrations-Erweiterungen spezifisch für openDesk, insbesondere zur Integration der Komponenten untereinander und zur Umsetzung des notwendigen Brandings. Die entsprechenden Hersteller sind mit Verweis auf den Sourcecode des zugehörigen Integrationspaketes der folgenden Liste zu entnehmen:

- Nextcloud: [integration_swp](https://gitlab.opencode.de/bmi/opendesk/component-code/file/nextcloud/nextcloud_app_integration_swp)
- OX App Suite 8: [Public Sector Extensions](https://gitlab.opencode.de/bmi/opendesk/component-code/groupware/appsuite8/extensions-public-sector)
- OpenProject: [openDesk plugin](https://gitlab.opencode.de/bmi/opendesk/component-code/project-management/openproject/openproject-opendesk-plugin)
- XWiki: [SWP Overlay](https://gitlab.opencode.de/bmi/opendesk/component-code/knowledge-management/xwiki/xwiki_swp_overlay)

## Anwendungsintegration- und Bereitstellung

Die Integration der Artefakte aus der Anwendungs- und Integrationsentwicklung findet im Projektrahmen von openDesk statt und beinhaltet im Kern die Deploymentautomatisierung, mit deren Hilfe eine Referenzinstallation von openDesk vorgenommen werden kann.

Die Deploymentautomatisierung soll eine möglichst niedrigschwellige Probenutzung von openDesk ermöglichen, ist jedoch nicht für die Nutzung im Produktivbetrieb vorgesehen, da
- die eingesetzten Community-Versionen der Open-Source Komponenten
  - nur über Community-Support verfügen und
  - zumindest teilweise nicht für den skalierten Betrieb ausgelegt sind;
- die eingesetzten "Notwendigen Services" (siehe Abschnitt "Komponenten von openDesk") im Produktivbetrieb durch externe Services zu ersetzen sind.

Neben der Deploymentautomatisierung werden weitere Projektarbeitsergebnisse auf Open CoDE bereitgestellt, wie SBOMs, CVE-Scan-Ergebnisse und Betriebsdokumentation.

# Komponenten von openDesk

Die folgende Tabelle gibt einen Überblick über die im Referenzdeployment enthaltenen Komponenten von openDesk.

- **Kernkomponenten**: Community Produkte als funktionale Basis von openDesk.
- **Notwendige Services**: Z.B. Datenbanken, werden beispielhaft in der Deploymentautomatisierung bereitgestellt, um einen Probebetrieb von openDesk zu ermöglichen. Für eine produktive Nutzung ist eine hochverfügbare und skalierbare Version des jeweiligen Service heranzuziehen.
- **Externe/optionale Services**: Für eine Probenutzung von openDesk nicht zwingend notwendige Services, die auch nicht im Rahmen des Deployments bereitgestellt werden.

| Kernkomponente              | Notwendige Services | Externe/optionale Services                 |
| --------------------------- | ------------------- | ------------------------------------------ |
| Collabora                   | ICAP                | Mail-Relay[^2] (optional)                  |
| CryptPad                    | MariaDB             | PKI/CA[^3] (optional)                      |
| Element                     | Memcached           | Persistent Volumes mit RWO/RWX Support[^4] |
| Jitsi                       | MinIO/S3            |                                            |
| Nextcloud                   | Postfix             |                                            |
| OpenProject                 | PostgreSQL          |                                            |
| OX AppSuite                 | Redis               |                                            |
| OX Dovecot                  | TURN/STUN[^5]       |                                            |
| Univention Corporate Server |                     |                                            |
| XWiki                       |                     |                                            |

Weitere Details zur Deploymentautomatisierung und den Voraussetzungen für den Betrieb von openDesk sind der [README.md der Deploymentautomatisierung](https://gitlab.opencode.de/bmi/opendesk/deployment/sovereign-workplace/-/blob/main/README.md) zu entnehmen.

## Helm Charts und Container-Images

Die Deploymentautomatisierung bedient sich Helm Charts und Container-Images, die überwiegend direkt von den Herstellern kommen, jedoch auch in den [zugehörigen Registries](https://gitlab.opencode.de/bmi/opendesk/components) auf Open CoDE bereitgestellt werden[^6].

## Weitere Liefergegenstände

Eine Übersicht zu weiteren Liefergegenständen kann den [Release Notes des Releases 23.12](./23.12/README.md) und zukünftiger Releases entnommen werden.

# Releases

Ein Release von openDesk besteht aus den aktuellsten Community Versionen der Herstellerprodukte, die durch die Deploymentautomatisierung integriert werden. Nach erfolgreicher Absolvierung der Quality Gates (z.B. Integrationstestautomatisierung) kann ein Release bereitgestellt werden.

Um Stabilität zu gewährleisten werden die jeweils aktuellsten Versionen jedoch nicht automatisch in openDesk berücksichtigt, statt dessen kommt Version Pinning zum Einsatz, so dass Aktualisierungen von Komponenten bewusst/explizit erfolgen müssen. In Kombination mit dem laufenden Ausbau der Integrationstests aktualisieren wir die Komponenten-Versionen jedoch kurzfristig nach deren Veröffentlichung, um von neuer Funktionalität, Verbesserungen sowie Korrekturen zu profitieren.

Ein Release von openDesk manifestiert sich damit aus einem getesteten Stand der Deploymentautomatisierung, der seinerseits spezifische Versionen der Komponenten referenziert.

Bei der Entwicklung halten wir uns dabei an zwei international gebräuchliche Konventionen:
- Versionierung basierend auf dem [Semantic Versioning Standard](https://semver.org/) Standard.
- Commits basierend auf dem [Conventional Commits](https://www.conventionalcommits.org/) Standard.

Offizielle Releases werden über ein explizites Tagging erzeugt, welches das gängige `<Jahr>.<Monat>.<Patch>` Muster nutzt, z.B. `23.12`.

# Rückmeldungen und Beteiligung

## Rückmeldungen

Die an openDesk beteiligten Personen freuen sich über Feedback auf verschiedenen Kanälen:

- [Open CoDE Diskussionsbereich](https://discourse.opencode.de/t/souveraener-arbeitsplatz-projekt-351/): Für allgemeines Feedback, Fragen, Hinweise und Wünsche.
- [Issue Board des Info-Repositories](https://gitlab.opencode.de/bmi/opendesk/info/-/issues): Fachliche Anforderungen an openDesk und auftretende Fehler bei der Verwendung.
- [Issue Board der Deploymentautomatisierung](https://gitlab.opencode.de/bmi/opendesk/deployment/opendesk/-/issues): Feedback zur Deploymentautomatisierung und ihrer Dokumentation sowie Fehlern und Erweiterungswünschen.

Insbesondere bei Themen die in einer einzelnen Fachkomponente verortet sind, stehen natürlich auch die Upstream-Kanäle der Hersteller zur Verfügung, wie z.B. das [BugBounty Programm von Nextcloud](https://hackerone.com/nextcloud), das [Hilfeforum von Univention](https://help.univention.com/) oder der [Matrix-Kanal von XWiki](https://dev.xwiki.org/xwiki/bin/view/Community/Chat).

Besteht Unsicherheit, welcher Kanal der geeignetste ist, bitte das Thema [im Diskussionsbereich auf Open CoDE platzieren](https://discourse.opencode.de/t/souveraener-arbeitsplatz-projekt-351/) platzieren, so dass wir uns um eine Antwort kümmern bzw. das Thema in dem richtigen Kanal unterbringen können.

## Beteiligung

### Anwendungs- und Integrationsentwicklung

Code-Anpassungen, z.B. Merge- / Pull-Requests, an den aufgeführten Produkten und deren zugehörigen Integrationspaketen können jeweils über die Upstream-Repositories der Hersteller und deren zugehörige Contribution-Prozesse durchgeführt werden. In der Kurzbeschreibung des jeweiligen Repositories auf Open CoDE wird das zugehörige Upstream-Repo benannt.

### Anwendungsintegration- und Bereitstellung

**ACHTUNG:** Alle für das Projekt erstellten (Code)Zeilen werden zwingend unter die Apache 2.0 Lizenz gestellt. Zudem bedarf es der Rechteabtretung die sich an den [Open CoDE Vorgaben](https://wikijs.opencode.de/de/Hilfestellungen_und_Richtlinien/CLA_DCO) orientiert. Aktuell befinden wir uns noch in der Erarbeitung dieses Prozesses für openDesk und können daher noch keine Merge Requests von Personen die nicht Teil des Projektes sind annehmen. Siehe auch [CONTRIBUTING.md](./CONTRIBUTING.md).

An der [Deployment-](https://gitlab.opencode.de/bmi/opendesk/deployment/sovereign-workplace) und [Testautomatisierung](https://gitlab.opencode.de/bmi/opendesk/deployment/e2e-tests) kann sich unmittelbar über Merge Requests beteiligt werden.

Registrierte Benutzer auf Open CoDE können über [Forks](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) Inhalte zum Projekt beisteuern. Projektmitglieder haben im Regelfall `Developer`-Berechtigungen in den Repositories und können über Branches und Merge-Requests Beistellungen leisten.

Weitere Details können der [Beschreibung des Entwicklungsworkflows](https://gitlab.opencode.de/bmi/opendesk/deployment/sovereign-workplace/-/blob/main/docs/workflow.md) entnommen werden.

# Fußnoten

[^1]: Details zum Mirroring der Sourcecode Repositories sind dem [Repository des Mirror-Skriptes](https://gitlab.opencode.de/bmi/opendesk/component-code/pull-mirror) zu entnehmen.

[^2]: Für den externen Mailversand.

[^3]: Zur Nutzung der S/MIME-Fähigkeit der Open-Xchange AppSuite.

[^4]: Um Storage für verschiedene Komponenten abzubilden. Im Rahmen der Entwicklung wird NFS genutzt, [Alternativen](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#access-modes) sind aber ebenso möglich. Ein nicht skalierendes Deployment kommt auch nur mit RWO Storage aus.

[^5]: Der STUN/TURN-Server ist nicht Teil der Deploymentautomatisierung, ein Beispiel eines `coturn`-Deployments ist im Repository [opendesk-base](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-base/-/tree/main/helmfile/apps/coturn) zu finden.

[^6]: Die Artefakte werden auch auf Open CoDE bereitgestellt, sobald sie notwendigen Compliance-Anforderungen, wie z.B. die Lizenzcompliance, erfüllen.
