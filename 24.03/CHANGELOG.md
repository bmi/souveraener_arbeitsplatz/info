<!--
SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->

<h1>openDesk 24.03 – Changelog</h1>

* [Disclaimer](#disclaimer)
* [CryptPad](#cryptpad)
  * [Accessibility](#accessibility)
* [Fileshare](#fileshare)
  * [Containerization](#containerization)
* [Groupware](#groupware)
  * [Dovecot Deployment](#dovecot-deployment)
  * [Email Categories](#email-categories)
  * [S/MIME-OX Guard](#smime-ox-guard)
* [Meeting and Collaboration](#meeting-and-collaboration)
  * [Accessibility](#accessibility-1)
* [Weboffice](#weboffice)
  * [Online Optimization](#online-optimization)
  * [Writer Interoperability](#writer-interoperability)
* [Portal and Identity Management](#portal-and-identity-management)
  * [Authorization policy](#authorization-policy)
  * [Containerization](#containerization-1)
* [Knowledgemanagement](#knowledgemanagement)
  * [Accessibility](#accessibility-2)
  * [Authentification](#authentification)
  * [Usability](#usability)

# Disclaimer

The changelog summarizes work packages that have been addressed by the suppliers in context of the openDesk project.

We do not include all changes within the upstream products, that can be found on the respective product websites.
For reference please find a list of the upstream products included in openDesk 24.03 in the
[release notes](README.md#upstream-products).

For reference please check the changelog of the previous release [openDesk 23.12](../23.12/CHANGELOG.md).

# CryptPad

## Accessibility

Accessibility has been improved to make CryptPad more compliant with accessibility requirements.

# Fileshare

## Containerization

The Nextcloud is containerized.

# Groupware

## Dovecot Deployment

Dovecot and the Build Scripts are now available on Open Code.

## Email Categories

Emails can now be assigned to categories, as before it had been possible for tasks, meetings and contacts.

## S/MIME-OX Guard

Implementation of the encryption of Emails by private certificate

# Meeting and Collaboration

## Accessibility

Accessibility has been improved to make the videoconferences more compliant with accessibility requirements.

# Weboffice

## Online Optimization

Improvement of

- tile pre-caching/rendering to avoid flickering
- using the zstd dictionary in order to speed up compression
- memory management and cache clearing allocator
- recalculating based on the user's view port
- reducing unnecessary invalidation

## Writer Interoperability

Multi-page floating tables and a first-page header and footer from and to docx documents are now provided.

# Portal and Identity Management

## Authorization policy

The Authorization Service has been provided. It can now be used in the IAM modules in order to manage users and groups.

## Containerization

The UCS and IAM have been containerized.

# Knowledgemanagement

## Accessibility

Accessibility has been improved to make XWiki more compliant with accessibility requirements.

## Authentification

The authentication mechanism has been improved and the synchronization between users and groups within XWiki and IAM has been enabled.

## Usability

The real time integration in form-based documents has been provided.
