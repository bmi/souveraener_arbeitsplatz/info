<!--
SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->

<h1>QA Report - System test</h1>

* [Identified deviations](#identified-deviations)
* [Test set: System](#test-set-system)
  * [Overview](#overview)
  * [Test cases](#test-cases)

# Identified deviations

| ID      | Priority | Result | Details                                                                                               |
| ------- | -------- | ------ | ----------------------------------------------------------------------------------------------------- |
| SY-0005 | Normal   | Skip   | Function not (yet) available in openDesk. Currently there is not admin role für Open-Xchange planned. |
| SY-0006 | Normal   | Skip   | See SY-0005                                                                                           |
| SY-0007 | Normal   | Skip   | See SY-0005                                                                                           |
| SY-0008 | Normal   | Skip   | See SY-0005                                                                                           |
| SY-0022 | High     | Skip   | Collabora Offline PoC is not part of the platform.                                                    |
| SY-0023 | High     | Skip   | See SY-0022                                                                                           |
| SY-0024 | High     | Skip   | See SY-0022                                                                                           |
| SY-0025 | High     | Skip   | See SY-0022                                                                                           |
| SY-0026 | High     | Skip   | See SY-0022                                                                                           |

```mermaid
%%{init: {'themeVariables': { 'pie1': '#5e27dd', 'pie2': '#0c3ff3', 'pie3': '#ff529e', 'pie4': '#00ffcd', 'pie5': '#ffc700', 'pie6': '#52c1ff', 'pie7': '#adb3bc'}}}%%
pie showData
    title Deviation distribution
    "Successful": 17
    "Skipped cases": 9
    "Failed test cases": 0
```

# Test set: System

## Overview

```mermaid
%%{init: {'themeVariables': { 'pie1': '#5e27dd', 'pie2': '#0c3ff3', 'pie3': '#ff529e', 'pie4': '#00ffcd', 'pie5': '#ffc700', 'pie6': '#52c1ff', 'pie7': '#adb3bc'}}}%%
pie showData
    "Communication": 18
    "Productivity": 8
```

```mermaid
%%{init: {'themeVariables': { 'pie1': '#5e27dd', 'pie2': '#0c3ff3', 'pie3': '#ff529e', 'pie4': '#00ffcd', 'pie5': '#ffc700', 'pie6': '#52c1ff', 'pie7': '#adb3bc'}}}%%
pie showData
    title Test cases by priority
    "High":12
    "Low":0
    "Normal":14
```

## Test cases

| ID      | Category       | Priority | Content                                                                                                    |
| ------- | -------------- | -------- | ---------------------------------------------------------------------------------------------------------- |
| SY-0001 | Communincation | Normal   | Category has a unique name                                                                                 |
| SY-0002 | Communincation | Normal   | Category has a color assigned                                                                              |
| SY-0003 | Communincation | Normal   | Category has a symbol assigned                                                                             |
| SY-0004 | Communincation | Normal   | Assign multiple categories to an email                                                                     |
| SY-0005 | Communincation | Normal   | Delete category as admin                                                                                   |
| SY-0006 | Communincation | Normal   | Create category as admin                                                                                   |
| SY-0007 | Communincation | Normal   | Modify category as admin                                                                                   |
| SY-0008 | Communincation | Normal   | User can make use of admin created category                                                                |
| SY-0009 | Communincation | Normal   | Search emails using categories                                                                             |
| SY-0010 | Communincation | Normal   | Modify category as user                                                                                    |
| SY-0011 | Communincation | Normal   | Create category as user                                                                                    |
| SY-0012 | Communincation | Normal   | All categories assigned to an email are shown to the user                                                  |
| SY-0013 | Communincation | High     | Select and book resources                                                                                  |
| SY-0014 | Communincation | High     | Get feedback on a managed resource                                                                         |
| SY-0015 | Communincation | High     | Show availablity of a resource                                                                             |
| SY-0016 | Communincation | High     | Modify a resource within in context of a (calendar) meeting                                                |
| SY-0017 | Communincation | Normal   | Verify user’s OX Guard S/MIME configuration                                                                |
| SY-0018 | Communincation | Normal   | Setup user’s OX Guard S/MIME configuration                                                                 |
| SY-0019 | Productivity   | High     | Writer – Table is rendered properly despite page breaks                                                    |
| SY-0020 | Productivity   | High     | Writer – Frame content is rendered properly despite page breaks                                            |
| SY-0021 | Productivity   | High     | Writer – Modifications page header/footer are applied to all pages                                         |
| SY-0022 | Productivity   | High     | Collabora-Offline PoC – Application is available                                                           |
| SY-0023 | Productivity   | High     | Collabora-Offline PoC – Create a new document in offline mode                                              |
| SY-0024 | Productivity   | High     | Collabora-Offline PoC – Modify existing documents from Nextcloud                                           |
| SY-0025 | Productivity   | High     | Collabora-Offline PoC – Synchronize offline documens when switching status to online                       |
| SY-0026 | Productivity   | High     | Collabora-Offline PoC – Synchronize offline documens when switching status to online – Multi user scenario |
